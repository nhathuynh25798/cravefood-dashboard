import React, { lazy } from 'react';
import { Link, Router, Redirect } from '@reach/router';
import { Result } from 'antd';

const HomePage = lazy(() => import('./pages/HomePage'));
const CategoryListPage = lazy(() => import('./pages/CategoryListPage'));
const ProductListPage = lazy(() => import('./pages/ProductListPage'));
const BannerPage = lazy(() => import('./pages/BannerPage'));
const OrderListPage = lazy(() => import('./pages/OrderListPage'));
const LoginPage = lazy(() => import('./pages/LoginPage'));
const UserListPage = lazy(() => import('./pages/UserListPage'));

const ProfilePage = lazy(() => import('./pages/ProfilePage'));
const UserDetailPage = lazy(() => import('./pages/UserDetailPage'));
const ProductDetailPage = lazy(() => import('./pages/ProductDetailPage'));
const CategoryDetailPage = lazy(() => import('./pages/CategoriesDetailPage'));

const PageNotFound = () => (
  <Result
    status="404"
    title="404"
    subTitle="Hmm. Chúng tôi gặp khó khăn khi tìm trang web đó."
    extra={<Link to="/">Back Home</Link>}
  />
);

const Routes = () => {
  return (
    <Router
      style={{
        backgroundColor: '--var(tertiary)',
        width: '100%',
        maxWidth: '1920px',
      }}
    >
      <PageNotFound path="*" />
      <Redirect from="/" to="/home" noThrow />
      <HomePage path="home" />
      <CategoryListPage path="categories/:page" />
      <ProductListPage path="products/:page" />
      <OrderListPage path="orders/:page" />
      <UserListPage path="users/:page" />
      {/* <IngredientListPage path="ingredients" /> */}
      <BannerPage path="banner" />
      <ProductDetailPage path="product/:slug" />
      <CategoryDetailPage path="category/:slug" />
      <LoginPage path="login" />
      <UserDetailPage path="user/:id" />
      <ProfilePage path="profile" />
    </Router>
  );
};

export default Routes;
