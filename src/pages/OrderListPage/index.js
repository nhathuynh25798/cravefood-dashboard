import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import { useSelector } from 'react-redux';
import { navigate } from '@reach/router';

import OrderCard from 'components/OrderCard';

import 'antd/dist/antd.css';

const OrderListPage = () => {
  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <Row justify="end">
      <Col span={24}>
        <OrderCard />
      </Col>
    </Row>
  );
};

export default OrderListPage;
