import React, { useEffect, useState } from 'react';
import { Row, Col, Table, Button, Checkbox, Modal, Pagination } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, navigate, useParams } from '@reach/router';

import slugify from 'utils/slugify';
import { actionCreators } from 'store/category/category.meta';
import NotificationRemoveConfirm from 'components/NotificationRemoveConfirm';
import TitlePage from 'components/TitlePage';
import CategoryForm from 'components/CategoryForm';

import 'antd/dist/antd.css';
import './style.scss';

const CategoryListPage = () => {
  const dispatch = useDispatch();
  const { page } = useParams();

  const { categories = {} } = useSelector((store) => store.category);
  const { user = {} } = useSelector((store) => store.user);
  const { list = [], total } = categories;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const hideModal = () => {
    setIsModalVisible(false);
  };

  const fetchCategories = () => {
    dispatch(actionCreators.actFetchCategories(page));
  };

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  useEffect(() => {
    fetchCategories();
  }, [page]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (page) => {
    navigate(`${page}`);
  };

  const columns = [
    {
      title: 'Tên danh mục',
      dataIndex: ['name', 'slug'],
      key: 'name',
      sorter: (a, b) => a?.name?.length - b?.name?.length,
      defaultSortOrder: 'descend',
      render: (_, record) => (
        <Link to={`/category/${record?.slug}`} style={{ fontSize: '18px' }}>
          {record?.name?.toUpperCase()}
        </Link>
      ),
    },
    {
      title: 'Hiển thị cho người dùng',
      dataIndex: 'enable',
      key: 'enable',
      filters: [
        {
          text: 'Các danh mục sản phẩm hiển thị',
          value: 1,
        },
        {
          text: 'Các danh mục sản phẩm ẩn',
          value: 0,
        },
      ],
      onFilter: (value, record) => record.enable === value,
      render: (enable) => {
        return <Checkbox disabled checked={!!enable} />;
      },
    },
    {
      title: 'Hiển thị trên trang chủ',
      dataIndex: 'showHome',
      key: 'showHome',
      filters: [
        {
          text: 'Các danh mục sản phẩm hiển thị',
          value: 1,
        },
        {
          text: 'Các danh mục sản phẩm ẩn',
          value: 0,
        },
      ],
      onFilter: (value, record) => record.showHome === value,
      render: (showHome) => {
        return <Checkbox disabled checked={!!showHome} />;
      },
    },
    {
      title: 'Hiển thị trên menu',
      dataIndex: 'showMenu',
      key: 'showMenu',
      filters: [
        {
          text: 'Các danh mục sản phẩm hiểu thị',
          value: 1,
        },
        {
          text: 'Các danh mục sản phẩm ẩn',
          value: 0,
        },
      ],
      onFilter: (value, record) => record.showMenu === value,
      render: (showMenu) => {
        return <Checkbox disabled checked={!!showMenu} />;
      },
    },
    {
      title: 'Thao tác',
      key: 'operation',
      render: (_, record) => (
        <Row justify="center" gutter={[16, 0]}>
          <Col xs={12}>
            <Button type="ghost" className="button-action edit-button">
              <Link to={`/category/${record?.slug}`}>Sửa</Link>
            </Button>
          </Col>
          <Col xs={12}>
            <Button
              type="ghost"
              className="button-action delete-button"
              onClick={() => {
                return NotificationRemoveConfirm(() => {
                  dispatch(
                    actionCreators.actDeleteCategories({
                      id: record?.id,
                      page: page,
                    })
                  );
                });
              }}
            >
              Xoá
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const handleFinish = (values) => {
    const { id, name, slug, enable, showHome, showMenu, createBy } = values;
    const categoryNeedSave = {
      id,
      name: name.trim(),
      slug: slugify(slug),
      createBy,
      enable: enable ? 1 : 0,
      showHome: showHome ? 1 : 0,
      showMenu: showMenu ? 1 : 0,
    };
    dispatch(actionCreators.actCreateCategories(categoryNeedSave));
    hideModal();
  };

  return (
    <Row gutter={[0, 48]}>
      <Col xs={24}>
        <TitlePage title="Danh mục món ăn" />
      </Col>
      <Col xs={12} style={{ display: 'flex', alignItems: 'center' }}>
        <Row justify="end" style={{ width: '100%' }}>
          <Button type="primary" size="large" onClick={showModal}>
            Thêm danh mục
          </Button>
          <Modal
            width="70%"
            title={<TitlePage title="Danh mục món ăn" />}
            visible={isModalVisible}
            footer={null}
            onCancel={hideModal}
          >
            <CategoryForm onFinish={handleFinish} />
          </Modal>
        </Row>
      </Col>
      <Col xs={24}>
        <Table
          columns={columns}
          dataSource={list}
          rowKey="id"
          bordered
          pagination={false}
        />
      </Col>
      <Col span={24}>
        <Pagination
          defaultCurrent={1}
          total={total}
          pageSize={12}
          onChange={handleChangePage}
        />
      </Col>
    </Row>
  );
};
export default CategoryListPage;
