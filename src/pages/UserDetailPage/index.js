import React, { useEffect } from 'react';
import { useParams } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col } from 'antd';

import ProfileForm from 'components/AccountWrapper/ProfileForm';
import { actionCreator } from 'store/user/user.meta';

const UserDetailPage = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { byId = {} } = useSelector((store) => store.user);

  const fetchUserById = () => {
    dispatch(actionCreator.fetchUserById(id));
  };

  useEffect(() => {
    fetchUserById();
  }, [id]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Row>
      <Col span={24}>
        <ProfileForm user={byId} />
      </Col>
    </Row>
  );
};

export default UserDetailPage;
