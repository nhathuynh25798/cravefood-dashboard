import React, { useEffect } from 'react';
import { useParams } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';
import { navigate } from '@reach/router';
import { Row, Col } from 'antd';

import slugify from 'utils/slugify';
import ProductForm from 'components/ProductForm';
import { actionCreators as productActions } from 'store/product/product.meta';

import './style.scss';

const ProductDetailPage = () => {
  const dispatch = useDispatch();
  const { slug } = useParams();
  const { bySlug } = useSelector((store) => store.product);

  const fetchProductBySlug = () => {
    dispatch(productActions.actFetchProductBySlug(slug));
  };

  const scrollToTop = () => {
    window.scrollTo({ top: 0 });
  };

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [slug]);

  useEffect(() => {
    fetchProductBySlug();
    scrollToTop();
  }, [slug]); // eslint-disable-line react-hooks/exhaustive-deps

  const onFinish = (values) => {
    const {
      id,
      name,
      slug,
      enable,
      description,
      discount,
      smallValue,
      mediumValue,
      largeValue,
      createBy,
      uploads,
      selectItems = [],
      uploadImages = [],
    } = values;
    const productNeedSave = {
      id,
      name: name.trim(),
      slug: slugify(slug),
      description: description.trim(),
      images: uploads,
      discount,
      enable: enable ? 1 : 0,
      createBy,
      sizes: [
        {
          sizeId: 1,
          price: smallValue,
          ingredients: bySlug?.sizes?.filter((it) => it?.sizeId === 1)?.[0]
            ?.ingredients,
        },
        {
          sizeId: 2,
          price: mediumValue,
          ingredients: bySlug?.sizes?.filter((it) => it?.sizeId === 2)?.[0]
            ?.ingredients,
        },
        {
          sizeId: 3,
          price: largeValue,
          ingredient: bySlug?.sizes?.filter((it) => it?.sizeId === 3)?.[0]
            ?.ingredients,
        },
      ]?.filter((it) => it?.price > 0),
      categories: selectItems.map((it) => {
        return { id: it };
      }),
      files: uploadImages,
    };
    dispatch(productActions.actCreateProduct(productNeedSave));
  };

  return (
    <Row>
      <Col span={24}>
        <ProductForm bySlug={bySlug} onFinish={onFinish} />
      </Col>
    </Row>
  );
};

export default ProductDetailPage;
