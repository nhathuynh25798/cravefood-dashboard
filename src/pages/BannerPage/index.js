/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import UploadComponent from 'components/UploadComponent';
import TitlePage from 'components/TitlePage';
import { actionCreators } from 'store/banner/banner.meta';

const BannerPage = () => {
  require('dotenv').config();
  const dispatch = useDispatch();
  const { banners = [] } = useSelector((store) => store.banner);

  const [fileList, setFileList] = useState(banners);

  const fetchBanner = () => {
    dispatch(actionCreators.actFetchBanner());
    setFileList(banners);
  };

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [banners?.length, user]);

  useEffect(() => {
    fetchBanner();
  }, [banners?.length]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleAddBanner = (upload) => {
    dispatch(actionCreators.actAddBanner({ ...upload }));
  };

  const handleRemoveBanner = (remove) => {
    dispatch(
      actionCreators.actRemoveBanner({
        ...remove,
      })
    );
  };

  return (
    <Row>
      <Col xs={24}>
        <TitlePage title="Quản lý Banners" />
      </Col>
      <Col span={24}>
        <UploadComponent
          list={fileList}
          handleAddImage={handleAddBanner}
          handleRemoveImage={handleRemoveBanner}
        />
      </Col>
    </Row>
  );
};

export default BannerPage;
