import React, { useEffect } from 'react';
import { useParams } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';
import { navigate } from '@reach/router';
import { Row, Col } from 'antd';

import slugify from 'utils/slugify';
import CategoryForm from 'components/CategoryForm';
import { actionCreators } from 'store/category/category.meta';

const CategoryDetailPage = () => {
  const dispatch = useDispatch();
  const { slug } = useParams();
  const { bySlug } = useSelector((store) => store.category);

  const fetchCategoryBySlug = () => {
    dispatch(actionCreators.actFetchCategoriesBySlug(slug));
  };

  const scrollToTop = () => {
    window.scrollTo({ top: 0 });
  };

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [slug]);

  useEffect(() => {
    fetchCategoryBySlug();
    scrollToTop();
  }, [slug]); // eslint-disable-line react-hooks/exhaustive-deps

  const onFinish = (values) => {
    const { id, name, slug, enable, showHome, showMenu, createBy } = values;
    const categoryNeedSave = {
      id,
      name: name.trim(),
      slug: slugify(slug),
      createBy,
      enable: enable ? 1 : 0,
      showHome: showHome ? 1 : 0,
      showMenu: showMenu ? 1 : 0,
    };
    dispatch(actionCreators.actCreateCategories(categoryNeedSave));
  };

  return (
    <Row>
      <Col span={24}>
        <CategoryForm bySlug={bySlug} onFinish={onFinish} />
      </Col>
    </Row>
  );
};

export default CategoryDetailPage;
