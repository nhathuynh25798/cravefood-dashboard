import React, { useEffect } from 'react';
import { navigate } from '@reach/router';
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';

import ProfileForm from 'components/AccountWrapper/ProfileForm';

const ProfilePage = () => {
  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <Row>
      <Col span={24}>
        <ProfileForm user={user} />
      </Col>
    </Row>
  );
};

export default ProfilePage;
