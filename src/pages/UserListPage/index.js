import React, { useEffect, useState } from 'react';
import { Row, Col, Table, Button, Modal, Pagination } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, navigate, useParams } from '@reach/router';

import { actionCreator } from 'store/user/user.meta';
import NotificationRemoveConfirm from 'components/NotificationRemoveConfirm';
import TitlePage from 'components/TitlePage';
import RegisterForm from 'components/AccountWrapper/RegisterForm';

import 'antd/dist/antd.css';

const UserListPage = () => {
  require('dotenv').config();
  const dispatch = useDispatch();
  const { page } = useParams();

  const { users = {} } = useSelector((store) => store.user);
  const { list = [], total } = users;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const hideModal = () => {
    setIsModalVisible(false);
  };

  const fetchUsers = () => {
    dispatch(actionCreator.fetchUsers(page));
  };

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, user]);

  useEffect(() => {
    fetchUsers();
  }, [page]); // eslint-disable-line react-hooks/exhaustive-deps

  const columns = [
    {
      title: 'Địa chỉ email',
      dataIndex: ['email', 'id'],
      key: 'email',
      render: (_, record) => (
        <Link to={`/user/${record?.id}`} style={{ fontSize: '18px' }}>
          {record?.email}
        </Link>
      ),
    },
    {
      title: 'Họ Tên',
      dataIndex: ['name', 'id'],
      key: 'name',
      sorter: (a, b) => a?.name?.length - b?.name?.length,
      defaultSortOrder: 'descend',
      render: (_, record) => (
        <Link to={`/user/${record?.id}`} style={{ fontSize: '18px' }}>
          {record?.name}
        </Link>
      ),
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
      render: (_, record) => (
        <span style={{ fontSize: '18px' }}>{record?.phone}</span>
      ),
    },

    {
      title: 'Thao tác',
      key: 'operation',
      render: (_, record) => (
        <Row justify="center" gutter={[16, 0]}>
          <Col xs={12}>
            <Button type="ghost" className="button-action edit-button">
              <Link to={`/user/${record?.id}`}>Sửa</Link>
            </Button>
          </Col>
          <Col xs={12}>
            <Button
              type="ghost"
              className="button-action delete-button"
              onClick={() => {
                return NotificationRemoveConfirm(() => {
                  dispatch(
                    actionCreator.deleteUser({
                      id: record?.id,
                      page: page,
                    })
                  );
                });
              }}
            >
              Xoá
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const handleChangePage = (page) => {
    navigate(`${page}`);
  };

  return (
    <Row gutter={[0, 48]}>
      <Col xs={12}>
        <TitlePage title="Người dùng" />
      </Col>
      <Col xs={12} style={{ display: 'flex', alignItems: 'center' }}>
        <Row justify="end" style={{ width: '100%' }}>
          <Button type="primary" size="large" onClick={showModal}>
            Thêm người dùng
          </Button>
          <Modal
            width="70%"
            title={<TitlePage title="Người dùng" />}
            visible={isModalVisible}
            footer={null}
            onCancel={hideModal}
          >
            <RegisterForm onSuccess={hideModal} />
          </Modal>
        </Row>
      </Col>
      <Col xs={24}>
        <Table
          columns={columns}
          dataSource={list}
          rowKey="id"
          bordered
          pagination={false}
        />
      </Col>
      <Col span={24}>
        <Pagination
          defaultCurrent={1}
          total={total}
          pageSize={12}
          onChange={handleChangePage}
        />
      </Col>
    </Row>
  );
};

export default UserListPage;
