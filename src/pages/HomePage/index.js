import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import { navigate } from '@reach/router';

import { useSelector } from 'react-redux';

import TitlePage from 'components/TitlePage';
import 'antd/dist/antd.css';
import './style.scss';

const HomePage = () => {
  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <Row>
      <Col span={24}>
        <TitlePage title="Xin chào Admin." />
      </Col>
    </Row>
  );
};
export default HomePage;
