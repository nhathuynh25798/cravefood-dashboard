import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Table,
  Button,
  Image,
  Checkbox,
  Modal,
  Pagination,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, navigate, useParams } from '@reach/router';

import slugify from 'utils/slugify';
import { actionCreators } from 'store/product/product.meta';
import NotificationRemoveConfirm from 'components/NotificationRemoveConfirm';
import TitlePage from 'components/TitlePage';
import { actionCreators as productActions } from 'store/product/product.meta';
import ProductForm from 'components/ProductForm';

import 'antd/dist/antd.css';
import './style.scss';

const ProductListPage = () => {
  require('dotenv').config();
  const dispatch = useDispatch();
  const { page } = useParams();

  const { products = {} } = useSelector((store) => store.product);
  const { list = [], total } = products;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const hideModal = () => {
    setIsModalVisible(false);
  };

  const fetchProduct = () => {
    dispatch(actionCreators.actFetchProducts(page));
  };

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!user?.id) {
      navigate('/login');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  useEffect(() => {
    fetchProduct();
  }, [page]); // eslint-disable-line react-hooks/exhaustive-deps

  const columns = [
    {
      title: 'Tên',
      dataIndex: ['name', 'slug'],
      key: 'name',
      sorter: (a, b) => a?.name?.length - b?.name?.length,
      defaultSortOrder: 'descend',
      render: (_, record) => (
        <Link to={`/product/${record?.slug}`} style={{ fontSize: '18px' }}>
          {record?.name?.toUpperCase()}
        </Link>
      ),
    },
    {
      title: 'Hình ảnh',
      dataIndex: 'images',
      key: 'images',
      render: (images) => (
        <Image
          src={process.env.REACT_APP_BACKEND_URL + images?.[0]?.content}
          width={140}
        />
      ),
    },
    {
      title: 'Hiển thị cho người dùng',
      dataIndex: 'enable',
      key: 'enable',
      filters: [
        {
          text: 'Các sản phẩm hiển thị',
          value: 1,
        },
        {
          text: 'Các sản phẩm ẩn',
          value: 0,
        },
      ],
      onFilter: (value, record) => record.enable === value,
      render: (enable) => <Checkbox disabled checked={!!enable} />,
    },
    {
      title: 'Thao tác',
      key: 'operation',
      render: (_, record) => (
        <Row justify="center" gutter={[16, 0]}>
          <Col xs={12}>
            <Button type="ghost" className="button-action edit-button">
              <Link to={`/product/${record?.slug}`}>Sửa</Link>
            </Button>
          </Col>
          <Col xs={12}>
            <Button
              type="ghost"
              className="button-action delete-button"
              onClick={() => {
                return NotificationRemoveConfirm(() => {
                  dispatch(
                    actionCreators.actDeleteProduct({
                      id: record?.id,
                      page: page,
                    })
                  );
                });
              }}
            >
              Xoá
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const handleChangePage = (page) => {
    navigate(`${page}`);
  };

  const handleFinish = (values) => {
    const {
      id,
      name,
      slug,
      enable,
      description,
      discount,
      smallValue,
      mediumValue,
      largeValue,
      createBy,
      uploads,
      selectItems = [],
      uploadImages = [],
      removeImages = [],
    } = values;
    const productNeedSave = {
      id,
      name: name.trim(),
      slug: slugify(slug),
      description: description.trim(),
      images: uploads,
      discount,
      enable: enable ? 1 : 0,
      createBy,
      sizes: [
        {
          sizeId: 1,
          price: smallValue,
        },
        {
          sizeId: 2,
          price: mediumValue,
        },
        {
          sizeId: 3,
          price: largeValue,
        },
      ]?.filter((it) => it?.price > 0),
      categories: selectItems.map((it) => {
        return { id: it };
      }),
      files: uploadImages,
      removeFiles: removeImages,
    };
    dispatch(productActions.actCreateProduct(productNeedSave));
    hideModal();
  };

  return (
    <Row gutter={[0, 48]}>
      <Col xs={12}>
        <TitlePage title="Món ăn" />
      </Col>
      <Col xs={12} style={{ display: 'flex', alignItems: 'center' }}>
        <Row justify="end" style={{ width: '100%' }}>
          <Button type="primary" size="large" onClick={showModal}>
            Thêm sản phẩm
          </Button>
          <Modal
            width="70%"
            title={<TitlePage title="Món ăn" />}
            visible={isModalVisible}
            footer={null}
            onCancel={hideModal}
          >
            <ProductForm onFinish={handleFinish} />
          </Modal>
        </Row>
      </Col>
      <Col xs={24}>
        <Table
          columns={columns}
          dataSource={list}
          rowKey="id"
          bordered
          pagination={false}
        />
      </Col>
      <Col span={24}>
        <Pagination
          defaultCurrent={1}
          total={total}
          pageSize={12}
          onChange={handleChangePage}
        />
      </Col>
    </Row>
  );
};
export default ProductListPage;
