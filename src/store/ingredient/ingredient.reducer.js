import { types } from './ingredient.meta';
import { handleActions } from 'redux-actions';

const defaultIngredientState = {
  ingredients: {
    list: [],
    total: 0,
  },
  byId: {},
};

const getIngredientsSuccess = (state, action) => ({
  ...state,
  ingredients: action.payload,
});

const getIngredientByIdSuccess = (state, action) => ({
  ...state,
  byId: action.payload,
});

export default handleActions(
  {
    [types.FETCH_INGREDIENT_SUCCESS]: getIngredientsSuccess,
    [types.FETCH_INGREDIENT_BY_ID_SUCCESS]: getIngredientByIdSuccess,
  },
  defaultIngredientState
);
