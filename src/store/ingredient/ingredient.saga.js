import { takeLatest, put, call, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import {
  getIngredients,
  getIngredientById,
  createIngredient,
  deleteIngredient,
} from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';
import { types, actionCreators } from './ingredient.meta';

function* sagaGetIngredients(action) {
  const { body, httpStatus } = yield call(getIngredients, action.payload);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchIngredientsSuccess(body));
  }
}

function* sagaFetchIngredientById(action) {
  const { body } = yield call(getIngredientById, action.payload);

  yield put(actionCreators.actFetchIngredientByIdSuccess(body));
}

function* sagaCreateIngredient(action) {
  const { body } = yield call(createIngredient, action.payload);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchIngredients(1));
    yield navigate('/ingredients/1');
  }
}

function* sagaDeleteIngredient(action) {
  const { id, page } = action.payload;
  const { body } = yield call(deleteIngredient, id);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchIngredients(page));
    yield navigate(`/ingredients/${page}`);
  }
}

// Monitoring Sagas
function* ingredientMonitor() {
  yield all([
    takeLatest(
      types.FETCH_INGREDIENT_BY_ID,
      sagaErrorWrapper(sagaFetchIngredientById)
    ),
    takeLatest(types.FETCH_INGREDIENT, sagaErrorWrapper(sagaGetIngredients)),

    takeLatest(types.CREATE_INGREDIENT, sagaErrorWrapper(sagaCreateIngredient)),
    takeLatest(types.DELETE_INGREDIENT, sagaErrorWrapper(sagaDeleteIngredient)),
  ]);
}

export default ingredientMonitor;
