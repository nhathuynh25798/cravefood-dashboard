import { createAction } from 'redux-actions';

export const types = {
  FETCH_INGREDIENT: 'FETCH_INGREDIENT',
  FETCH_INGREDIENT_SUCCESS: 'FETCH_INGREDIENT_SUCCESS',

  FETCH_INGREDIENT_BY_ID: 'FETCH_INGREDIENT_BY_ID',
  FETCH_INGREDIENT_BY_ID_SUCCESS: 'FETCH_INGREDIENT_BY_ID_SUCCESS',

  SEARCH_INGREDIENT: 'SEARCH_INGREDIENT',
  SEARCH_INGREDIENT_SUCCESS: 'SEARCH_INGREDIENT_SUCCESS',

  DELETE_INGREDIENT: 'DELETE_INGREDIENT',
  CREATE_INGREDIENT: 'CREATE_INGREDIENT',
};

export const actionCreators = {
  actCreateIngredient: createAction(types.CREATE_INGREDIENT),
  actDeleteIngredient: createAction(types.DELETE_INGREDIENT),

  actFetchIngredients: createAction(types.FETCH_INGREDIENT),
  actFetchIngredientsSuccess: createAction(types.FETCH_INGREDIENT_SUCCESS),

  actFetchIngredientBySlug: createAction(types.FETCH_INGREDIENT_BY_ID),
  actFetchIngredientBySlugSuccess: createAction(
    types.FETCH_INGREDIENT_BY_ID_SUCCESS
  ),
};
