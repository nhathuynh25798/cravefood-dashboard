import { takeLatest, put, call, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import {
  getCategories,
  getCategoriesBySlug,
  createCategories,
  deleteCategories,
} from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';
import { types, actionCreators } from './category.meta';

function* sagaGetCategories(action) {
  const { body, httpStatus } = yield call(getCategories, action.payload);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchCategoriesSuccess(body));
  }
}

function* sagaFetchCategoriesBySlug(action) {
  const { body } = yield call(getCategoriesBySlug, action.payload);

  yield put(actionCreators.actFetchCategoriesBySlugSuccess(body));
}

function* sagaCreateCategories(action) {
  const { body } = yield call(createCategories, action.payload);

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreators.actFetchCategories(1));
  yield navigate(`/categories/1`);
}

function* sagaDeleteCategories(action) {
  const { id, page } = action.payload;
  const { body } = yield call(deleteCategories, id);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchCategories(page));
    yield navigate(`/categories/${page}`);
  }
}

// Monitoring Sagas
function* productMonitor() {
  yield all([
    takeLatest(
      types.FETCH_CATEGORIES_BY_SLUG,
      sagaErrorWrapper(sagaFetchCategoriesBySlug)
    ),
    takeLatest(types.FETCH_CATEGORIES, sagaErrorWrapper(sagaGetCategories)),

    takeLatest(types.CREATE_CATEGORIES, sagaErrorWrapper(sagaCreateCategories)),
    takeLatest(types.DELETE_CATEGORIES, sagaErrorWrapper(sagaDeleteCategories)),
  ]);
}

export default productMonitor;
