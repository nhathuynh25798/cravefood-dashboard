import { createAction } from 'redux-actions';

export const types = {
  FETCH_CATEGORIES: 'FETCH_CATEGORIES',
  FETCH_CATEGORIES_SUCCESS: 'FETCH_CATEGORIES_SUCCESS',

  FETCH_CATEGORIES_BY_SLUG: 'FETCH_CATEGORIES_BY_SLUG',
  FETCH_CATEGORIES_BY_SLUG_SUCCESS: 'FETCH_CATEGORIES_BY_SLUG_SUCCESS',

  SEARCH_CATEGORIES: 'SEARCH_CATEGORIES',
  SEARCH_CATEGORIES_SUCCESS: 'SEARCH_CATEGORIES_SUCCESS',

  DELETE_CATEGORIES: 'DELETE_CATEGORIES',
  CREATE_CATEGORIES: 'CREATE_CATEGORIES',
};

export const actionCreators = {
  actCreateCategories: createAction(types.CREATE_CATEGORIES),
  actDeleteCategories: createAction(types.DELETE_CATEGORIES),

  actFetchCategories: createAction(types.FETCH_CATEGORIES),
  actFetchCategoriesSuccess: createAction(types.FETCH_CATEGORIES_SUCCESS),

  actFetchCategoriesBySlug: createAction(types.FETCH_CATEGORIES_BY_SLUG),
  actFetchCategoriesBySlugSuccess: createAction(
    types.FETCH_CATEGORIES_BY_SLUG_SUCCESS
  ),
};
