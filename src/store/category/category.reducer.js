import { types } from './category.meta';
import { handleActions } from 'redux-actions';

const defaultCategoriesState = {
  categories: {
    list: [],
    total: 0,
  },
  bySlug: {},
};

const getCategoriesSuccess = (state, action) => ({
  ...state,
  categories: action.payload,
});

const getCategoriesBySlugSuccess = (state, action) => ({
  ...state,
  bySlug: action.payload,
});

export default handleActions(
  {
    [types.FETCH_CATEGORIES_SUCCESS]: getCategoriesSuccess,
    [types.FETCH_CATEGORIES_BY_SLUG_SUCCESS]: getCategoriesBySlugSuccess,
  },
  defaultCategoriesState
);
