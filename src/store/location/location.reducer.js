import { types } from './location.meta';
import { handleActions } from 'redux-actions';

const defaultLocationState = {
  provinces: [],
};

const getProvincesSuccess = (state, action) => ({
  ...state,
  provinces: action.payload,
});

export default handleActions(
  {
    [types.FETCH_PROVINCES_SUCCESS]: getProvincesSuccess,
  },
  defaultLocationState
);
