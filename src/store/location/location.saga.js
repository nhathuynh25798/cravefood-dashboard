import { takeLatest, put, call, all } from 'redux-saga/effects';

import { getProvinces } from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';

import { types, actionCreators } from './location.meta';

function* sagaFetchProvinces(action) {
  const { body, httpStatus } = yield call(getProvinces);
  if (httpStatus === 200) {
    yield put(actionCreators.getProvincesSuccess(body));
  }
}

// Monitoring Sagas
function* locationMonitor() {
  yield all([
    takeLatest(types.FETCH_PROVINCES, sagaErrorWrapper(sagaFetchProvinces)),
  ]);
}

export default locationMonitor;
