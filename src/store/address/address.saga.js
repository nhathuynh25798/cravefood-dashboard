import { takeLatest, put, all } from 'redux-saga/effects'

import { types, actionCreators } from './address.meta'
import { sagaErrorWrapper } from 'utils/error'

function* sagaAddAddress(action) {
  const data = action.payload
  yield put(actionCreators.actAddAddressSuccess(data))
}

function* sagaRemoveAddress(action) {
  const data = action.payload
  yield put(actionCreators.actRemoveAddressSuccess(data))
}

// Monitoring Sagas
function* cartMonitor() {
  yield all([
    takeLatest(types.ADD_ADDRESS, sagaErrorWrapper(sagaAddAddress)),
    takeLatest(types.REMOVE_ADDRESS, sagaErrorWrapper(sagaRemoveAddress)),
  ])
}

export default cartMonitor
