import { createAction } from 'redux-actions'

export const types = {
  ADD_ADDRESS: 'ADD_ADDRESS',
  REMOVE_ADDRESS: 'REMOVE_ADDRESS',

  ADD_ADDRESS_SUCCESS: 'ADD_ADDRESS_SUCCESS',
  REMOVE_ADDRESS_SUCCESS: 'REMOVE_ADDRESS_SUCCESS',
}

export const actionCreators = {
  actAddAddress: createAction(types.ADD_ADDRESS),
  actRemoveAddress: createAction(types.REMOVE_ADDRESS),

  actAddAddressSuccess: createAction(types.ADD_ADDRESS_SUCCESS),
  actRemoveAddressSuccess: createAction(types.REMOVE_ADDRESS_SUCCESS),
}
