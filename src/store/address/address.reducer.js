import { types } from './address.meta';
import { handleActions } from 'redux-actions';
import { notification } from 'antd';

const defaultAddressState = {
  items: [],
  total: 0,
};

const addAddressSuccess = (state, action) => {
  const addressItem = action.payload;
  const itemsClone = [...state.items];
  const index = itemsClone.findIndex((x) => x.id === addressItem.id);

  if (index !== -1) {
    itemsClone[index] = addressItem;
    notification.success({
      message: 'Chỉnh sửa địa chỉ thành công',
    });
    if (addressItem.checkbox === true) {
      itemsClone.forEach((item) => (item.checkbox = false));
      itemsClone[index].checkbox = true;
      const itemClone = itemsClone[0];
      itemsClone[0] = itemsClone[index];
      itemsClone[index] = itemClone;
      notification.success({
        message: 'Đặt thành địa chỉ mặc định thành công',
      });
    }
  } else {
    if (addressItem.checkbox === true) {
      itemsClone.unshift(addressItem);
      notification.success({
        message: 'Thêm địa chỉ thành công',
      });
      itemsClone.forEach((item) => (item.checkbox = false));
      const itemIndex = itemsClone.findIndex((x) => x.id === addressItem.id);
      itemsClone[itemIndex].checkbox = true;
      notification.success({
        message: 'Đặt thành địa chỉ mặc định thành công',
      });
    } else {
      itemsClone.push(addressItem);
      notification.success({
        message: 'Thêm địa chỉ thành công',
      });
    }
  }
  return {
    ...state,
    items: itemsClone,
    total: itemsClone.length,
  };
};

const removeAddressSuccess = (state, action) => {
  const addressId = action.payload;
  const itemsClone = [...state.items];
  const index = itemsClone.findIndex((x) => x.id === addressId);
  if (index !== -1) {
    if (itemsClone[index].checkbox === true) {
      notification.error({
        message: 'Địa chỉ mặc định không thể xóa',
      });
    } else {
      itemsClone.splice(index, 1);
    }
  }
  return {
    ...state,
    items: itemsClone,
    total: itemsClone.length,
  };
};

export default handleActions(
  {
    [types.ADD_ADDRESS_SUCCESS]: addAddressSuccess,
    [types.REMOVE_ADDRESS_SUCCESS]: removeAddressSuccess,
  },
  defaultAddressState
);
