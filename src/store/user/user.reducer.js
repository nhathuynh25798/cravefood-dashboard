import { handleActions } from 'redux-actions';
import { types } from './user.meta';

const initialState = {
  user: null,
  users: {
    list: [],
    total: 0,
  },
  byId: {},
};

const fetchUserSuccess = (state, action) => ({
  ...state,
  users: action.payload,
});

const fetchUserByIdSuccess = (state, action) => ({
  ...state,
  byId: action.payload,
});

const loginSuccess = (state, { payload }) => ({
  ...state,
  user: payload.user,
});

const logoutSuccess = (state) => ({
  user: null,
});

export default handleActions(
  {
    [types.LOGIN_SUCCESS]: loginSuccess,
    [types.LOGOUT_SUCCESS]: logoutSuccess,

    [types.FETCH_USERS_SUCCESS]: fetchUserSuccess,
    [types.FETCH_USER_BY_ID_SUCCESS]: fetchUserByIdSuccess,
  },
  initialState
);
