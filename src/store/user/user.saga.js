import { takeLatest, call, put, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import jwtDecode from 'jwt-decode';

import { types, actionCreator } from './user.meta';
import { actionCreator as pageActionCreator } from '../page/page.meta';

import {
  fetchUsers,
  fetchUserById,
  deleteUser,
  customerRegister,
  customerLogin,
  updateUser,
} from 'api/apiRouter';
import cookie from 'js-cookie';
import { sagaErrorWrapper } from 'utils/error';

function* sageFetchUsers(action) {
  const { body } = yield call(fetchUsers, action.payload);

  yield put(actionCreator.fetchUsersSuccess(body));
}

function* sagaFetchUserById(action) {
  const { body } = yield call(fetchUserById, action.payload);

  yield put(actionCreator.fetchUserByIdSuccess(body));
}

function* sagaDeleteUser(action) {
  const { id, page } = action.payload;
  const { body } = yield call(deleteUser, id);

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreator.fetchUsers(page));
  yield navigate(`/users/${page}`);
}

function* sagaRegister(action) {
  let { email, phone, password } = action.payload;
  const { body } = yield call(customerRegister, {
    email,
    phone,
    password,
  });
  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreator.fetchUsers(1));
  yield navigate('/users/1');
}

function* sagaLogin(action) {
  let { email, password } = action.payload;
  const { body } = yield call(customerLogin, {
    email,
    password,
  });

  const { token } = body;
  const { exp, user } = jwtDecode(token);

  cookie.set('token', token, { expires: exp });

  yield put(
    actionCreator.loginSuccess({
      user: {
        ...user,
        loginIn: true,
      },
    })
  );
  yield call(notification.success, {
    message: 'Đăng nhập thành công!',
  });
  yield put(pageActionCreator.setLoading(false));

  yield navigate('/');
}

function* sagaLogout(action) {
  cookie.remove('token');
  yield put(actionCreator.logoutSuccess());
  yield call(notification.success, {
    message: 'Đăng xuất thành công!',
  });
  yield navigate('/');
}

// == update user ==
function* sagaUpdateUser(action) {
  const { id, name, phone, gender, birthday } = action.payload;
  const { body } = yield call(updateUser, {
    id,
    name,
    phone,
    gender,
    birthday,
  });

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreator.fetchUserById(id));
}

export default function* userSaga() {
  yield all([
    takeLatest(types.LOGIN, sagaErrorWrapper(sagaLogin)),
    takeLatest(types.REGISTER, sagaErrorWrapper(sagaRegister)),
    takeLatest(types.LOGOUT, sagaErrorWrapper(sagaLogout)),
    takeLatest(types.UPDATE_USER, sagaErrorWrapper(sagaUpdateUser)),

    takeLatest(types.DELETE_USER, sagaErrorWrapper(sagaDeleteUser)),
    takeLatest(types.FETCH_USERS, sagaErrorWrapper(sageFetchUsers)),
    takeLatest(types.FETCH_USER_BY_ID, sagaErrorWrapper(sagaFetchUserById)),
  ]);
}
