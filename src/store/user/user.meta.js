import { createAction } from 'redux-actions';

export const types = {
  REGISTER: 'REGISTER',

  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  SET_TOKEN: 'SET_TOKEN',

  LOGOUT: 'LOGOUT',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',

  UPDATE_USER: 'UPDATE_USER',
  UPDATE_USER_SUCCESS: 'UPDATE_USER_SUCCESS',

  DELETE_USER: 'DELETE_USER',

  FETCH_USERS: 'FETCH_USERS',
  FETCH_USERS_SUCCESS: 'FETCH_USERS_SUCCESS',

  FETCH_USER_BY_ID: 'FETCH_USERS',
  FETCH_USER_BY_ID_SUCCESS: 'FETCH_USERS_BY_ID_SUCCESS',
};

export const actionCreator = {
  register: createAction(types.REGISTER),

  login: createAction(types.LOGIN),
  loginSuccess: createAction(types.LOGIN_SUCCESS),

  setToken: createAction(types.SET_TOKEN),

  logout: createAction(types.LOGOUT),
  logoutSuccess: createAction(types.LOGOUT_SUCCESS),

  updateUser: createAction(types.UPDATE_USER),
  updateUserSuccess: createAction(types.UPDATE_USER_SUCCESS),

  deleteUser: createAction(types.DELETE_USER),

  fetchUsers: createAction(types.FETCH_USERS),
  fetchUsersSuccess: createAction(types.FETCH_USERS_SUCCESS),

  fetchUserById: createAction(types.FETCH_USER_BY_ID),
  fetchUserByIdSuccess: createAction(types.FETCH_USER_BY_ID_SUCCESS),
};
