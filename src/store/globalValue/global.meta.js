import { createAction } from 'redux-actions';

export const types = {
  FETCH_GLOBAL_VALUE: 'FETCH_GLOBAL_VALUE',
  FETCH_GLOBAL_VALUE_SUCCESS: 'FETCH_GLOBAL_VALUE_SUCCESS',
};

export const actionCreators = {
  actFetchGlobalValue: createAction(types.FETCH_GLOBAL_VALUE),
  getGlobalValueSuccess: createAction(types.FETCH_GLOBAL_VALUE_SUCCESS),
};
