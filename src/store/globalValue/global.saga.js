import { takeLatest, put, call, all } from 'redux-saga/effects';

import { getGlobalValue } from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';

import { types, actionCreators } from './global.meta';

function* sagaFetchGlobalValue(action) {
  const { body, httpStatus } = yield call(getGlobalValue);
  if (httpStatus === 200) {
    yield put(actionCreators.getGlobalValueSuccess(body));
  }
}

// Monitoring Sagas
function* globalValueMonitor() {
  yield all([
    takeLatest(
      types.FETCH_GLOBAL_VALUE,
      sagaErrorWrapper(sagaFetchGlobalValue)
    ),
  ]);
}

export default globalValueMonitor;
