import { createAction } from "redux-actions";

export const types = {
  FETCH_BLOGS: "FETCH_BLOGS",
  FETCH_BLOGS_SUCCESS: "FETCH_BLOGS_SUCCESS",

  FETCH_BLOG_BY_SLUG: "FETCH_BLOG_BY_SLUG",
  FETCH_BLOG_BY_SLUG_SUCCESS: "FETCH_BLOG_BY_SLUG_SUCCESS",
};

export const actionCreators = {
  fetchBlogs: createAction(types.FETCH_BLOGS),
  fetchBlogsSuccess: createAction(types.FETCH_BLOGS_SUCCESS),

  fetchBlogBySlug: createAction(types.FETCH_BLOG_BY_SLUG),
  fetchBlogBySlugSuccess: createAction(types.FETCH_BLOG_BY_SLUG_SUCCESS),
  // actFetchProductBySlug: createAction(types.REGISTER_SUCCESS),
  // actFetchRelatedProduct: createAction(types.CHANGE_STEP),
  // actFetchProductByCategory: createAction(types.FETCH_PRODUCT_BY_CATEGORY),
  // actAddProductCompare: createAction(types.ADD_PRODUCT_COMPARE),
  // actRemoveProductCompare: createAction(types.REMOVE_PRODUCT_COMPARE),
  // actLikedProduct: createAction(types.LIKE_PRODUCT),
  // actGetProductsLiked: createAction(types.GET_PRODUCTS_LIKED),
  // actRatingProduct: createAction(types.RATING_PRODUCT),
};
