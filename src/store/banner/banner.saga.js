import { takeLatest, put, call, all } from 'redux-saga/effects';
import { notification } from 'antd';

import {
  getBanners,
  addBanner,
  deleteBanner,
  updateBanner,
} from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';

import { types, actionCreators } from './banner.meta';

function* sagaFetchBanner(action) {
  const { body, httpStatus } = yield call(getBanners);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchBannerSuccess(body));
  }
}

function* sagaAddBanner(action) {
  const { body } = yield call(addBanner, { ...action.payload });

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreators.actFetchBanner());
}

function* sageUpdateBanner(action) {
  const { body } = yield call(updateBanner, action.payload);

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreators.actFetchBanner());
}

function* sagaDeleteBanner(action) {
  const { body } = yield call(deleteBanner, { ...action.payload });

  yield call(notification.success, {
    message: body?.message,
  });

  yield put(actionCreators.actFetchBanner());
}

// Monitoring Sagas
function* BannerMonitor() {
  yield all([
    takeLatest(types.FETCH_BANNER, sagaErrorWrapper(sagaFetchBanner)),
    takeLatest(types.ADD_BANNER, sagaErrorWrapper(sagaAddBanner)),
    takeLatest(types.REMOVE_BANNER, sagaErrorWrapper(sagaDeleteBanner)),
    takeLatest(types.UPDATE_BANNER, sagaErrorWrapper(sageUpdateBanner)),
  ]);
}

export default BannerMonitor;
