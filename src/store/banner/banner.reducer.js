import { types } from './banner.meta';
import { handleActions } from 'redux-actions';

const defaultBannerState = {
  banners: [],
};

const getBannerSuccess = (state, action) => ({
  ...state,
  banners: action.payload,
});

export default handleActions(
  {
    [types.FETCH_BANNER_SUCCESS]: getBannerSuccess,
  },
  defaultBannerState
);
