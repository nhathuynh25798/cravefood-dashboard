import { createAction } from 'redux-actions';

export const types = {
  FETCH_BANNER: 'FETCH_BANNER',
  FETCH_BANNER_SUCCESS: 'FETCH_BANNER_SUCCESS',

  ADD_BANNER: 'ADD_BANNER',
  REMOVE_BANNER: 'REMOVE_BANNER',
  UPDATE_BANNER: 'UPDATE_BANNER',
};

export const actionCreators = {
  actFetchBanner: createAction(types.FETCH_BANNER),
  actFetchBannerSuccess: createAction(types.FETCH_BANNER_SUCCESS),

  actAddBanner: createAction(types.ADD_BANNER),
  actRemoveBanner: createAction(types.REMOVE_BANNER),
  actUpdateBanner: createAction(types.UPDATE_BANNER),
};
