import { createAction } from 'redux-actions';

export const types = {
  FETCH_PRODUCT: 'FETCH_PRODUCT',
  FETCH_PRODUCT_SUCCESS: 'FETCH_PRODUCT_SUCCESS',

  FETCH_PRODUCT_BY_SLUG: 'FETCH_PRODUCT_BY_SLUG',
  FETCH_PRODUCT_BY_SLUG_SUCCESS: 'FETCH_PRODUCT_BY_SLUG_SUCCESS',

  SEARCH_PRODUCT: 'SEARCH_PRODUCT',
  SEARCH_PRODUCT_SUCCESS: 'SEARCH_PRODUCT_SUCCESS',

  DELETE_PRODUCT: 'DELETE_PRODUCT',

  // //image
  // ADD_IMAGE_PRODUCT: 'ADD_IMAGE_PRODUCT',
  // ADD_IMAGE_PRODUCT_SUCCESS: 'ADD_IMAGE_PRODUCT_SUCCESS',

  // REMOVE_IMAGE_PRODUCT: 'REMOVE_IMAGE_PRODUCT',
  // REMOVE_IMAGE_PRODUCT_SUCCESS: 'REMOVE_IMAGE_PRODUCT_SUCCESS',

  // // dish size
  // ADD_DISHSIZE_PRODUCT: 'ADD_DISHSIZE_PRODUCT',
  // ADD_DISHSIZE_PRODUCT_SUCCESS: 'ADD_DISHSIZE_PRODUCT_SUCCESS',

  // REMOVE_DISHSIZE_PRODUCT: 'REMOVE_DISHSIZE_PRODUCT',
  // REMOVE_DISHSIZE_PRODUCT_SUCCESS: 'REMOVE_DISHSIZE_PRODUCT_SUCCESS',

  // // dish_ingredient
  // ADD_DISH_INGREDIENT_PRODUCT: 'ADD_DISH_INGREDIENT_PRODUCT',
  // ADD_DISH_INGREDIENT_PRODUCT_SUCCESS: 'ADD_DISH_INGREDIENT_PRODUCT_SUCCESS',

  // REMOVE_DISH_INGREDIENT_PRODUCT: 'REMOVE_DISH_INGREDIENT_PRODUCT',
  // REMOVE_DISH_INGREDIENT_PRODUCT_SUCCESS:
  //   'REMOVE_DISH_INGREDIENT_PRODUCT_SUCCESS',

  CREATE_PRODUCT: 'CREATE_PRODUCT',
  // CREATE_PRODUCT_SUCCESS: 'CREATE_PRODUCT_SUCCESS',
};

export const actionCreators = {
  actCreateProduct: createAction(types.CREATE_PRODUCT),
  actDeleteProduct: createAction(types.DELETE_PRODUCT),

  actFetchProducts: createAction(types.FETCH_PRODUCT),
  actFetchProductsSuccess: createAction(types.FETCH_PRODUCT_SUCCESS),

  actFetchProductBySlug: createAction(types.FETCH_PRODUCT_BY_SLUG),
  actFetchProductBySlugSuccess: createAction(
    types.FETCH_PRODUCT_BY_SLUG_SUCCESS
  ),

  // // images
  // actAddImageProduct: createAction(types.ADD_IMAGE_PRODUCT),
  // actAddImageProductSuccess: createAction(types.ADD_IMAGE_PRODUCT_SUCCESS),
  // actRemoveImageProduct: createAction(types.REMOVE_IMAGE_PRODUCT),
  // actRemoveImageProductSuccess: createAction(
  //   types.REMOVE_IMAGE_PRODUCT_SUCCESS
  // ),

  // // dish size
  // actAddDishSizeProduct: createAction(types.ADD_DISHSIZE_PRODUCT),
  // actAddDishSizeProductSuccess: createAction(
  //   types.ADD_DISHSIZE_PRODUCT_SUCCESS
  // ),
  // actRemoveDishSizeProduct: createAction(types.REMOVE_DISHSIZE_PRODUCT),
  // actRemoveDishSizeProductSuccess: createAction(
  //   types.REMOVE_DISHSIZE_PRODUCT_SUCCESS
  // ),
  // // dish ingredient
  // actAddDishIngredientProduct: createAction(types.ADD_DISH_INGREDIENT_PRODUCT),
  // actAddDishIngredientProductSuccess: createAction(
  //   types.ADD_DISH_INGREDIENT_PRODUCT_SUCCESS
  // ),
  // actRemoveDishIngredientProduct: createAction(
  //   types.REMOVE_DISH_INGREDIENT_PRODUCT
  // ),
  // actRemoveDishIngredientProductSuccess: createAction(
  //   types.REMOVE_DISH_INGREDIENT_PRODUCT_SUCCESS
  // ),
};
