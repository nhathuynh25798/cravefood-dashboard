import { takeLatest, put, call, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import {
  getProducts,
  getProductBySlug,
  createProduct,
  deleteProduct,
} from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';
import { types, actionCreators } from './product.meta';

function* sagaGetProducts(action) {
  const { body, httpStatus } = yield call(getProducts, action.payload);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchProductsSuccess(body));
  }
}

function* sagaFetchProductBySlug(action) {
  const { body } = yield call(getProductBySlug, action.payload);

  yield put(actionCreators.actFetchProductBySlugSuccess(body));
}

function* sagaCreateProduct(action) {
  const { body } = yield call(createProduct, action.payload);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchProducts(1));
    yield navigate('/products/1');
  }
}

function* sagaDeleteProduct(action) {
  const { id, page } = action.payload;
  const { body } = yield call(deleteProduct, id);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchProducts(page));
    yield navigate(`/products/${page}`);
  }
}

// Monitoring Sagas
function* productMonitor() {
  yield all([
    takeLatest(
      types.FETCH_PRODUCT_BY_SLUG,
      sagaErrorWrapper(sagaFetchProductBySlug)
    ),
    takeLatest(types.FETCH_PRODUCT, sagaErrorWrapper(sagaGetProducts)),

    takeLatest(types.CREATE_PRODUCT, sagaErrorWrapper(sagaCreateProduct)),
    takeLatest(types.DELETE_PRODUCT, sagaErrorWrapper(sagaDeleteProduct)),
  ]);
}

export default productMonitor;
