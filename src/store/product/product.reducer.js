import { types } from './product.meta';
import { handleActions } from 'redux-actions';

const defaultProductState = {
  products: {
    list: [],
    total: 0,
  },
  bySlug: {},
  product: {},
  files: [],
  dishIngredients: [],
  dishSizes: [],
};

const getProductsSuccess = (state, action) => ({
  ...state,
  products: action.payload,
});

const getProductBySlugSuccess = (state, action) => ({
  ...state,
  bySlug: action.payload,
});

// // images
// const addImageProduct = (state, action) => {
//   const imagesClone = [...state.images];
//   const imageItem = { id: imagesClone.length, ...action.payload };
//   imagesClone.push(imageItem);
//   notification.success({
//     message: 'Thêm ảnh thành công',
//   });
//   return {
//     ...state,
//     images: imagesClone,
//   };
// };

// const removeImageProduct = (state, action) => {
//   const { id } = action.payload;
//   const imagesClone = [...state.images];
//   const index = imagesClone.findIndex((x) => x.id === id);
//   if (index !== -1) {
//     imagesClone.splice(index, 1);
//   }
//   notification.success({
//     message: 'Xoá ảnh thành công',
//   });
//   return {
//     ...state,
//     images: imagesClone,
//   };
// };

// // dish size
// const addDishSizeProduct = (state, action) => {
//   const dishSizeItem = action.payload;
//   const dishSizesClone = [...state.dishSizes];
//   const index = dishSizeItem.findIndex(
//     (x) =>
//       x.productId === dishSizeItem.productId && x.sizeId === dishSizeItem.sizeId
//   );
//   if (index !== -1) {
//     dishSizesClone[index] = dishSizeItem;
//     notification.success({
//       message: 'Cập nhật size thành công',
//     });
//   } else {
//     dishSizesClone.push(dishSizeItem);
//     notification.success({
//       message: 'Thêm size thành công',
//     });
//   }
//   return {
//     ...state,
//     dishSizes: dishSizesClone,
//   };
// };

// const removeDishSizeProduct = (state, action) => {
//   const dishSizeItem = action.payload;
//   const dishSizesClone = [...state.dishSizes];
//   const index = dishSizeItem.findIndex(
//     (x) =>
//       x.productId === dishSizeItem.productId && x.sizeId === dishSizeItem.sizeId
//   );
//   if (index !== -1) {
//     dishSizesClone.splice(index, 1);
//   }
//   return {
//     ...state,
//     dishSizes: dishSizesClone,
//   };
// };

// // dish ingredient
// const addDishIngredientProduct = (state, action) => {
//   const dishIngredientItem = action.payload;
//   const dishIngredientClone = [...state.dishIngredients];
//   const index = dishIngredientClone.findIndex(
//     (x) =>
//       x.productId === dishIngredientItem.productId &&
//       x.sizeId === dishIngredientItem.sizeId &&
//       x.ingredientId === dishIngredientItem.ingredientId
//   );
//   if (index !== -1) {
//     dishIngredientClone[index] = dishIngredientItem;
//     notification.success({
//       message: 'Cập nhật size thành công',
//     });
//   } else {
//     dishIngredientClone.push(dishIngredientItem);
//     notification.success({
//       message: 'Thêm nguyên liệu thành công',
//     });
//   }
//   return {
//     ...state,
//     dishIngredients: dishIngredientClone,
//   };
// };

// const removeDishIngredientProduct = (state, action) => {
//   const dishIngredientItem = action.payload;
//   const dishIngredientClone = [...state.dishIngredients];
//   const index = dishIngredientClone.findIndex(
//     (x) =>
//       x.productId === dishIngredientItem.productId &&
//       x.sizeId === dishIngredientItem.sizeId &&
//       x.ingredientId === dishIngredientItem.ingredientId
//   );
//   if (index !== -1) {
//     dishIngredientItem.splice(index, 1);
//   }
//   return {
//     ...state,
//     dishIngredients: dishIngredientClone,
//   };
// };

// const createProduct = (state, action) => {
//   return {
//     ...state,
//     product: {},
//     files: [],
//     dishIngredients: [],
//     dishSizes: [],
//   };
// };

export default handleActions(
  {
    [types.FETCH_PRODUCT_SUCCESS]: getProductsSuccess,
    [types.FETCH_PRODUCT_BY_SLUG_SUCCESS]: getProductBySlugSuccess,

    // // images
    // [types.ADD_IMAGE_PRODUCT_SUCCESS]: addImageProduct,
    // [types.REMOVE_IMAGE_PRODUCT_SUCCESS]: removeImageProduct,

    // // dish size
    // [types.ADD_DISHSIZE_PRODUCT_SUCCESS]: addDishSizeProduct,
    // [types.REMOVE_DISHSIZE_PRODUCT_SUCCESS]: removeDishSizeProduct,

    // // dish ingredient
    // [types.ADD_DISH_INGREDIENT_PRODUCT_SUCCESS]: addDishIngredientProduct,
    // [types.REMOVE_DISH_INGREDIENT_PRODUCT_SUCCESS]: removeDishIngredientProduct,

    // [types.CREATE_PRODUCT_SUCCESS]: createProduct,
  },
  defaultProductState
);
