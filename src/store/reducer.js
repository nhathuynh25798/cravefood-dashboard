import { combineReducers } from 'redux';

import product from './product/product.reducer';
import blog from './blog/blog.reducer';
import address from './address/address.reducer';
import category from './category/category.reducer';
import page from './page/page.reducer';
import location from './location/location.reducer';
import banner from './banner/banner.reducer';
import globalValue from './globalValue/global.reducer';
import order from './order/order.reducer';
import user from './user/user.reducer';

export default combineReducers({
  product,
  category,
  page,
  blog,
  address,
  location,
  banner,
  globalValue,
  order,
  user,
});
