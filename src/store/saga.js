import { all, fork } from 'redux-saga/effects';

import product from './product/product.saga';
import blogSaga from './blog/blog.saga';
import addressSaga from './address/address.saga';
import category from './category/category.saga';
import location from './location/location.saga';
import banner from './banner/banner.saga';
import globalValue from './globalValue/global.saga';
import order from './order/order.saga';
import user from './user/user.saga';

export default function* rootSaga() {
  yield all([
    fork(product),
    fork(blogSaga),
    fork(category),
    fork(addressSaga),
    fork(location),
    fork(banner),
    fork(globalValue),
    fork(order),
    fork(user),
  ]);
}
