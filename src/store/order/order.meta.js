import { createAction } from 'redux-actions';

export const types = {
  FETCH_ORDER: 'FETCH_ORDER',
  FETCH_ORDER_SUCCESS: 'FETCH_ORDER_SUCCESS',

  FETCH_ORDER_BY_ID: 'FETCH_ORDER_BY_ID',
  FETCH_ORDER_BY_ID_SUCCESS: 'FETCH_ORDER_BY_ID_SUCCESS',

  DELETE_ORDER: 'DELETE_ORDER',
};

export const actionCreators = {
  actDeleteOrder: createAction(types.DELETE_ORDER),

  actFetchOrders: createAction(types.FETCH_ORDER),
  actFetchOrdersSuccess: createAction(types.FETCH_ORDER_SUCCESS),

  actFetchOrderBySlug: createAction(types.FETCH_ORDER_BY_ID),
  actFetchOrderBySlugSuccess: createAction(types.FETCH_ORDER_BY_ID_SUCCESS),
};
