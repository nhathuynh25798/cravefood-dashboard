import { types } from './order.meta';
import { handleActions } from 'redux-actions';

const defaultOrderState = {
  orders: {
    list: [],
    total: 0,
  },
  byId: {},
};

const getOrdersSuccess = (state, action) => ({
  ...state,
  orders: action.payload,
});

const getOrderByIdSuccess = (state, action) => ({
  ...state,
  byId: action.payload,
});

export default handleActions(
  {
    [types.FETCH_ORDER_SUCCESS]: getOrdersSuccess,
    [types.FETCH_ORDER_BY_ID_SUCCESS]: getOrderByIdSuccess,
  },
  defaultOrderState
);
