import { takeLatest, put, call, all } from 'redux-saga/effects';
import { notification } from 'antd';
import { navigate } from '@reach/router';
import { getOrders, getOrderById, deleteOrder } from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';
import { types, actionCreators } from './order.meta';

function* sagaGetOrders(action) {
  const { body, httpStatus } = yield call(getOrders, action.payload);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchOrdersSuccess(body));
  }
}

function* sagaFetchOrderById(action) {
  const { body } = yield call(getOrderById, action.payload);

  yield put(actionCreators.actFetchOrderByIdSuccess(body));
}

function* sagaDeleteOrder(action) {
  const { id, page } = action.payload;
  const { body } = yield call(deleteOrder, id);

  yield call(notification.success, {
    message: body?.message,
  });

  if (body?.status === 200) {
    yield put(actionCreators.actFetchOrders(page));
    yield navigate(`/orders/${page}`);
  }
}

// Monitoring Sagas
function* orderMonitor() {
  yield all([
    takeLatest(types.FETCH_ORDER_BY_ID, sagaErrorWrapper(sagaFetchOrderById)),
    takeLatest(types.FETCH_ORDER, sagaErrorWrapper(sagaGetOrders)),

    takeLatest(types.DELETE_ORDER, sagaErrorWrapper(sagaDeleteOrder)),
  ]);
}

export default orderMonitor;
