import React, { Suspense } from 'react'
import { Spin } from 'antd'

import LayoutAdmin from './layout'
import './App.less'

const App = () => {
  return (
    <Suspense
      fallback={
        <Spin
          size='large'
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100vh',
            color: 'var(--primary)',
          }}
        />
      }
    >
      <LayoutAdmin />
    </Suspense>
  )
}

export default App
