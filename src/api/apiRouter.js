import { buildRequest } from 'api';
import qs from 'qs';
import cookie from 'js-cookie';

// Product
export const getProducts = (page) =>
  buildRequest(`/product/page/${page}`, {
    method: 'GET',
  }).request();

export const getProductBySlug = (slug) =>
  buildRequest(`/product/${slug}`, { method: 'GET' }).request();

export const searchProduct = (keyword) =>
  buildRequest(`/product/find/${keyword}`, { method: 'GET' }).request();

export const createProduct = (body) =>
  buildRequest(`/product/save`, { method: 'POST' }).request({ data: body });

export const deleteProduct = (id) =>
  buildRequest(`/product/delete/${id}`, { method: 'POST' }).request();

// Blog
export const getBlogs = (params) => {
  return params
    ? buildRequest(`/blog?${qs.stringify(params)}`, { method: 'GET' }).request()
    : buildRequest(`/blog`, { method: 'GET' }).request();
};
export const getBlogBySlug = (slug) =>
  buildRequest(`/blog/${slug}`, { method: 'GET' }).request();

// Orders
export const getOrders = (page) =>
  buildRequest(`/order/page/${page}`, {
    method: 'GET',
  }).request();

export const getOrderById = (id) =>
  buildRequest(`/order/${id}`, { method: 'GET' }).request();

export const deleteOrder = (id) =>
  buildRequest(`/order/delete/${id}`, { method: 'POST' }).request();

// Category
export const getCategories = (page) =>
  buildRequest(`/category/page/${page}`, {
    method: 'GET',
  }).request();

export const getCategoriesBySlug = (slug) =>
  buildRequest(`/category/${slug}`, { method: 'GET' }).request();

export const searchCategories = (keyword) =>
  buildRequest(`/category/find/${keyword}`, { method: 'GET' }).request();

export const createCategories = (body) =>
  buildRequest(`/category/add`, { method: 'POST' }).request({ data: body });

export const deleteCategories = (id) =>
  buildRequest(`/category/delete/${id}`, { method: 'POST' }).request();

// ingredient
export const getIngredients = (page) =>
  buildRequest(`/ingredient/page/${page}`, {
    method: 'GET',
  }).request();

export const getIngredientsBySlug = (slug) =>
  buildRequest(`/ingredient/${slug}`, { method: 'GET' }).request();

export const searchIngredient = (keyword) =>
  buildRequest(`/ingredient/find/${keyword}`, { method: 'GET' }).request();

export const createIngredient = (body) =>
  buildRequest(`/ingredient/save`, { method: 'POST' }).request({ data: body });

export const deleteIngredient = (id) =>
  buildRequest(`/ingredient/delete/${id}`, { method: 'POST' }).request();

// Orders
//Location

// ==== province ====
export const getProvinces = (params) => {
  return params
    ? buildRequest(`/province/${qs.stringify(params)}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/province/`, { method: 'GET' }).request();
};

// ==== global ====
export const getGlobalValue = () => {
  return buildRequest(`/globalValue/admin`, { method: 'GET' }).request();
};

// =========== User Request ========
const token = cookie.get('token');

export const getUser = (id) =>
  buildRequest(`/user/getUser/${id}`, {
    headers: { token: token },
  }).request({ method: 'POST' });

export const updateUser = (data) =>
  buildRequest(`/user/update`, {
    headers: { token: token },
  }).request({ method: 'POST', data: data });

export const customerLogin = (data) =>
  buildRequest('/auth/login/admin', { method: 'POST' }).request({ data: data });

export const customerRegister = (data) =>
  buildRequest('/auth/register', { method: 'POST' }).request({ data: data });

export const fetchUsers = (page) =>
  buildRequest(`/user/page/${page}`, {
    method: 'GET',
  }).request();

export const fetchUserById = (id) =>
  buildRequest(`/user/${id}`, { method: 'GET' }).request();

export const deleteUser = (id) =>
  buildRequest(`/user/delete/${id}`, { method: 'POST' }).request();

// banner
export const getBanners = (params) => {
  return params
    ? buildRequest(`/banner/list-banner${qs.stringify(params)}`, {
        method: 'GET',
      }).request()
    : buildRequest(`/banner/list-banner`, { method: 'GET' }).request();
};

export const deleteBanner = (data) =>
  buildRequest(`/banner/delete/`, { method: 'POST' }).request({
    data: data,
  });

export const addBanner = (data) =>
  buildRequest(`/banner/add/`, { method: 'POST' }).request({
    data: data,
  });

export const updateBanner = (id) =>
  buildRequest(`/banner/update/${id}`, { method: 'POST' }).request();
// ============= SaS Package calculate =======
