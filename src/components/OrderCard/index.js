import React, { useEffect } from 'react';
import {
  Row,
  Col,
  Collapse,
  Tag,
  Image,
  Empty,
  Pagination,
  Button,
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, navigate, useParams } from '@reach/router';

import NotificationRemoveConfirm from 'components/NotificationRemoveConfirm';
import { actionCreators } from 'store/order/order.meta';
import { currencyFormat } from 'utils/currencyFormat';
import { calculateSalePrice } from 'utils/helper';
import './style.scss';

const { Panel } = Collapse;

const OrderCard = () => {
  const dispatch = useDispatch();
  require('dotenv').config();
  const { list = [], total } = useSelector((store) => store.order.orders);
  // const { user = {} } = useSelector((store) => store.user);
  const { page } = useParams();

  const fetchOrders = () => {
    dispatch(actionCreators.actFetchOrders(page));
  };

  useEffect(() => {
    // if (!!!user) {
    //   navigate('/');
    // } else {
    fetchOrders();
    // }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChangePage = (page) => {
    navigate(`${page}`);
  };

  const Header = (props) => {
    const {
      codeString,
      deliveryDate,
      createAt,
      cartTotalDiscountPrice,
      cartTotalOriginalPrice,
    } = props;

    return (
      <Row justify="space-between">
        <Col style={{ flex: '1 1 0%' }}>
          <Row gutter={[16, 0]}>
            <Col span={8}>
              <span
                style={{
                  fontSize: '2rem',
                  fontWeight: 500,
                  lineHeight: '1.5rem',
                  marginRight: '8px',
                }}
              >
                Đơn hàng:
              </span>
              <Tag color="green">{codeString}</Tag>
            </Col>

            <Col
              style={{
                display: 'flex',
                alignItems: 'center',
                flex: '1 1 0%',
              }}
            >
              <Row gutter={[0, 8]}>
                <Col>
                  <span
                    style={{
                      fontSize: '1.8rem',
                      fontWeight: 500,
                      lineHeight: '1.5rem',
                    }}
                  >
                    Ngày tạo: {createAt}
                  </span>
                </Col>
                <Col>
                  <span
                    style={{
                      fontSize: '1.8rem',
                      fontWeight: 500,
                      lineHeight: '1.5rem',
                    }}
                  >
                    Ngày giao hàng: {deliveryDate}
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>

        <Col>
          <Row
            gutter={[0, 24]}
            style={{
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Col span={24}>
              <span
                style={{
                  fontSize: '1.8rem',
                  fontWeight: 500,
                  lineHeight: '1.5rem',
                }}
              >
                Tiền thanh toán:
              </span>
            </Col>
            {cartTotalOriginalPrice === cartTotalDiscountPrice ? (
              <span
                style={{
                  fontSize: '30px',
                  fontWeight: 500,
                  color: 'var(--primary)',
                  lineHeight: '1.5rem',
                }}
              >
                {currencyFormat(cartTotalDiscountPrice)}
              </span>
            ) : (
              <Col span={24}>
                <span
                  style={{
                    fontSize: '30px',
                    fontWeight: 500,
                    color: 'var(--primary)',
                    lineHeight: '1.5rem',
                  }}
                >
                  {currencyFormat(cartTotalDiscountPrice)}
                </span>
                <span
                  style={{
                    marginLeft: '8px',
                    fontSize: '30px',
                    fontWeight: 500,
                    color: 'var(--primary)',
                    lineHeight: '1.5rem',
                    textDecoration: 'line-through',
                  }}
                >
                  {currencyFormat(cartTotalDiscountPrice)}
                </span>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    );
  };

  return (
    <Row justify="center" gutter={[0, 32]}>
      <Col span={24}>
        <Row justify="center">
          {list?.length > 0 ? (
            <Collapse className="collapse-order">
              {list?.map((it) => (
                <Panel
                  header={
                    <Header
                      {...{
                        codeString: it?.codeString,
                        deliveryDate: it?.deliveryDate,
                        createAt: it?.createAt,
                        cartTotalDiscountPrice: it?.cartTotalDiscountPrice,
                        cartTotalOriginalPrice: it?.cartTotalOriginalPrice,
                      }}
                    />
                  }
                  key={it?.id}
                >
                  <Row gutter={[0, 32]}>
                    <Col span={24}>
                      <Row gutter={[0, 16]}>
                        <Col xs={24} sm={24}>
                          <Row gutter={[0, 8]}>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Người nhận hàng:
                              </span>
                            </Col>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                {it?.name}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col xs={24} sm={24}>
                          <Row gutter={[0, 8]}>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Số điện thoại:
                              </span>
                            </Col>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                {it?.phone}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        <Col xs={24} sm={24}>
                          <Row gutter={[0, 16]}>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Địa chỉ:
                              </span>
                            </Col>
                            <Col span={24}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                {it?.detail}, {it?.ward}, {it?.district},
                                {it?.province}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                        {it?.addressNote && (
                          <Col xs={24} sm={24}>
                            <Row gutter={[0, 8]}>
                              <Col span={24}>
                                <span
                                  style={{
                                    fontSize: '1.8rem',
                                    fontWeight: 500,
                                    lineHeight: '1.5rem',
                                  }}
                                >
                                  Ghi chú về địa chỉ:
                                </span>
                              </Col>
                              <Col span={24}>
                                <span
                                  style={{
                                    fontSize: '1.8rem',
                                    fontWeight: 400,
                                    lineHeight: '1.5rem',
                                  }}
                                >
                                  {it?.addressNote}
                                </span>
                              </Col>
                            </Row>
                          </Col>
                        )}

                        {it?.note && (
                          <Col xs={24} sm={24}>
                            <Row gutter={[0, 8]}>
                              <Col span={24}>
                                <span
                                  style={{
                                    fontSize: '1.8rem',
                                    fontWeight: 500,
                                    lineHeight: '1.5rem',
                                  }}
                                >
                                  Ghi chú đơn hàng:
                                </span>
                              </Col>
                              <Col span={24}>
                                <span
                                  style={{
                                    fontSize: '1.8rem',
                                    fontWeight: 400,
                                    lineHeight: '1.5rem',
                                  }}
                                >
                                  {it?.note}
                                </span>
                              </Col>
                            </Row>
                          </Col>
                        )}
                      </Row>
                    </Col>

                    <Col span={24}>
                      <span
                        style={{
                          fontSize: '30px',
                          fontWeight: 600,
                          lineHeight: '2.25rem',
                        }}
                      >
                        Chi tiết món ăn
                      </span>
                    </Col>
                    {it?.items?.map((it, idx) => (
                      <Col
                        span={24}
                        key={idx}
                        style={{
                          padding: '15px',
                          margin: '8px 0 0 0',
                          borderRadius: '4px',
                          border: '1px solid #d9d9d9',
                        }}
                      >
                        <Row justify="space-between" gutter={[32, 32]}>
                          <Col xs={{ span: 7 }}>
                            <Image
                              src={
                                process.env.REACT_APP_BACKEND_URL + it?.image
                              }
                              alt="product-img"
                              width="100%"
                            />
                          </Col>

                          <Col xs={{ span: 17 }}>
                            <Row
                              align="top"
                              gutter={[0, 8]}
                              style={{ padding: '0 0.8rem' }}
                            >
                              <Col xs={{ span: 24 }}>
                                <Link to={`/mon-le/chi-tiet/${it?.slug}`}>
                                  <span
                                    style={{
                                      fontSize: '2rem',
                                      textTransform: 'uppercase',
                                      cursor: 'pointer',
                                    }}
                                  >
                                    {it?.name}
                                  </span>
                                </Link>
                              </Col>

                              {it?.sizes
                                ?.sort((a, b) => a?.sizeId - b?.sizeId)
                                ?.map((item, idx) => (
                                  <Col xs={{ span: 24 }} key={idx}>
                                    <Row justify="space-between">
                                      <Col
                                        style={{
                                          display: 'flex',
                                          alignItems: 'center',
                                        }}
                                      >
                                        <img
                                          src={
                                            process.env.REACT_APP_BACKEND_URL +
                                            item?.sizeImage
                                          }
                                          style={{
                                            width: '20px',
                                          }}
                                          alt="s-size"
                                        />
                                        <span style={{ fontSize: '20px' }}>
                                          &nbsp;
                                          <strong>x{item?.quantity}</strong>
                                        </span>
                                      </Col>
                                      <Col
                                        style={{
                                          display: 'flex',
                                          alignItems: 'center',
                                          flexWrap: 'wrap',
                                          flex: '1 1 0%',
                                          marginLeft: '0.8rem',
                                        }}
                                      >
                                        {!!it?.discount ? (
                                          <Row>
                                            <span
                                              style={{
                                                marginRight: '0.8rem',
                                                fontSize: '20px',
                                              }}
                                            >
                                              {currencyFormat(
                                                calculateSalePrice(
                                                  it?.discount,
                                                  item?.quantity * item?.price
                                                )
                                              )}
                                            </span>
                                            <span
                                              style={{
                                                color: 'rgb(120, 120, 120)',
                                                fontSize: '20px',
                                                textDecoration: 'line-through',
                                              }}
                                            >
                                              {currencyFormat(
                                                item?.quantity * item?.price
                                              )}
                                            </span>
                                          </Row>
                                        ) : (
                                          <Row>
                                            <span
                                              style={{
                                                marginRight: '0.8rem',
                                                fontSize: '20px',
                                              }}
                                            >
                                              {currencyFormat(
                                                item?.quantity * item?.price
                                              )}
                                            </span>
                                          </Row>
                                        )}
                                      </Col>
                                    </Row>
                                  </Col>
                                ))}
                            </Row>
                          </Col>

                          <Col span={24}>
                            <Row justify="end">
                              <span
                                style={{
                                  marginRight: '0.8rem',
                                  fontSize: '20px',
                                }}
                              >
                                Chi phí món ăn:&nbsp;
                                {currencyFormat(
                                  calculateSalePrice(
                                    it?.discount,
                                    it?.sizes?.reduce(
                                      (prev, curr) =>
                                        prev + curr?.quantity * curr?.price,
                                      0
                                    )
                                  )
                                )}
                              </span>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                    ))}

                    <Col span={24}>
                      <Row justify="end" gutter={[16, 32]}>
                        <Col span={24}>
                          <Row gutter={[32, 0]}>
                            <Col span={18} style={{ textAlign: 'right' }}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Chí phí vận chuyển:
                              </span>
                            </Col>
                            <Col span={6}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                  color: 'var(--primary)',
                                }}
                              >
                                {currencyFormat(39000)}
                              </span>
                            </Col>
                          </Row>
                        </Col>

                        <Col span={24}>
                          <Row gutter={[32, 0]}>
                            <Col span={18} style={{ textAlign: 'right' }}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Giá trị đơn hàng:
                              </span>
                            </Col>
                            <Col span={6}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                  color: 'var(--primary)',
                                }}
                              >
                                {currencyFormat(
                                  it?.cartTotalOriginalPrice - 39000
                                )}
                              </span>
                            </Col>
                          </Row>
                        </Col>

                        <Col span={24}>
                          <Row gutter={[32, 0]}>
                            <Col span={18} style={{ textAlign: 'right' }}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Số tiền cần thanh toán:
                              </span>
                            </Col>
                            <Col span={6}>
                              <span
                                style={{
                                  fontSize: '24px',
                                  fontWeight: 400,
                                  lineHeight: '1.5rem',
                                  color: 'var(--primary)',
                                }}
                              >
                                {currencyFormat(it?.cartTotalOriginalPrice)}
                              </span>
                            </Col>
                          </Row>
                        </Col>

                        <Col xs={24}>
                          <Row justify="center">
                            <Button
                              size="large"
                              type="ghost"
                              style={{ width: '50%' }}
                              className="button-action delete-button"
                              onClick={() => {
                                return NotificationRemoveConfirm(() => {
                                  dispatch(
                                    actionCreators.actDeleteOrder({
                                      id: it?.id,
                                      page: page,
                                    })
                                  );
                                });
                              }}
                            >
                              Xoá
                            </Button>
                          </Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Panel>
              ))}
            </Collapse>
          ) : (
            <Col span={24}>
              <Row justify="center">
                <Empty description={`Bạn không có đơn hàng  nào?`} />
              </Row>
            </Col>
          )}
        </Row>
      </Col>
      <Col span={24}>
        <Pagination
          defaultCurrent={1}
          total={total}
          pageSize={12}
          onChange={handleChangePage}
        />
      </Col>
    </Row>
  );
};

export default OrderCard;
