import React, { useEffect } from 'react';
import { Row, Col, Card, Form, Input, Button, Radio, Select } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch } from 'react-redux';
import moment from 'moment';

import { actionCreator } from 'store/user/user.meta';
import HeadPhoneNumber from 'components/AccountWrapper/HeadPhoneNumber';

import '../style.scss';
import 'antd/dist/antd.css';

const { Group } = Radio;
const { Option } = Select;

const ProfileForm = (props) => {
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const dispatch = useDispatch();

  const NEW_DATE_FORMAT = 'DD/MM/YYYY';

  const { user = {} } = props;

  useEffect(() => {
    if (user?.id) {
      setFieldsValue({
        ...user,
        date: moment(user?.birthday, NEW_DATE_FORMAT).date(),
        month: moment(user?.birthday, NEW_DATE_FORMAT).month(),
        year: moment(user?.birthday, NEW_DATE_FORMAT).year(),
      });
    } else {
      navigate('/users/1');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user?.id]);

  const onFinish = (value) => {
    let { name, phone, gender, year, month, date } = value;
    value.birthday = date && moment(value.birthday).date(date);
    value.birthday = month && moment(value.birthday).month(month);
    value.birthday = year && moment(value.birthday).year(year);
    dispatch(
      actionCreator.updateUser({
        id: user?.id,
        name: name?.trim(),
        phone: phone?.trim(),
        gender,
        birthday: String(moment(value.birthday).format('DD-MM-YYYY')),
      })
    );
  };

  return (
    <Card bordered={false}>
      <Form form={form} scrollToFirstError onFinish={onFinish}>
        <Row className="account-form__container">
          <Col span={24} className="account-form__item">
            <span>Họ và tên</span>
            <Form.Item
              name="name"
              rules={[{ required: true, message: 'Vui lòng điền họ và tên' }]}
              className="account-input"
            >
              <Input size="large" placeholder="Điền họ và tên" />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Email</span>
            <Form.Item
              name="email"
              rules={[
                {
                  type: 'email',
                  message: 'Email không hợp lệ',
                },
                {
                  required: true,
                  message: 'Vui lòng điền email',
                },
              ]}
              className="account-input"
            >
              <Input size="large" disabled placeholder="Điền email" />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Số điện thoại</span>
            <Form.Item
              name="phone"
              rules={[
                {
                  pattern: '^0[0-9]{9}$',
                  required: true,
                  message: 'Hãy nhập số điện thoại',
                },
              ]}
              className="account-input"
            >
              <Input
                size="large"
                addonBefore={<HeadPhoneNumber />}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Giới tính</span>
            <Form.Item name="gender" className="account-input">
              <Group>
                <Radio value={1}>Nam</Radio>
                <Radio value={2}>Nữ</Radio>
                <Radio value={3}>Khác</Radio>
              </Group>
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Ngày sinh</span>

            <Row type="flex" gutter={8}>
              <Col xs={8}>
                <Form.Item name="date">
                  <Select style={{ width: '100%' }}>
                    {Array(31)
                      .fill(0)
                      .map((_, i) => (
                        <Option key={i} value={i + 1}>
                          {i + 1 < 10 ? '0' + (i + 1) : i + 1}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={8}>
                <Form.Item name="month">
                  <Select style={{ width: '100%' }}>
                    {Array(12)
                      .fill(0)
                      .map((_, i) => (
                        <Option key={i} value={i}>
                          {i + 1 < 10 ? '0' + (i + 1) : i + 1}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={8}>
                <Form.Item name="year">
                  <Select style={{ width: '100%' }}>
                    {Array(2020 - 14 - 1930)
                      .fill(0)
                      .map((_, i) => (
                        <Option key={i} value={i + 1930}>
                          {i + 1930}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>

          <Col span={24} style={{ marginTop: '2.4rem' }}>
            <Button
              type="primary"
              size="large"
              className="button-form"
              style={{ width: '100%' }}
              htmlType="submit"
            >
              Cập nhật
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
export default ProfileForm;
