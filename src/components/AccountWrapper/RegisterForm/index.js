import React from 'react';
import { Row, Col, Card, Form, Input, Button } from 'antd';
import { useDispatch } from 'react-redux';

import { actionCreator } from 'store/user/user.meta';
import HeadPhoneNumber from '../HeadPhoneNumber';
import 'antd/dist/antd.css';
import './style.scss';
import '../style.scss';

const RegisterForm = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const onFinish = (values) => {
    props.onSuccess();
    dispatch(
      actionCreator.register({
        ...values,
      })
    );
  };

  return (
    <Card bordered>
      <Form form={form} scrollToFirstError onFinish={onFinish}>
        <Row className="account-form__container">
          <Col
            span={24}
            className="account-form__item"
            style={{ marginTop: '2.4rem' }}
          >
            <span className="register-label">Email</span>
            <Form.Item
              name="email"
              rules={[
                {
                  type: 'email',
                  message: 'Email không hợp lệ',
                },
                {
                  required: true,
                  message: 'Vui lòng điền email',
                },
              ]}
              className="account-input"
            >
              <Input size="large" placeholder="Điền email" />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Số điện thoại</span>
            <Form.Item
              name="phone"
              rules={[
                {
                  pattern: '^0[0-9]{9}$',
                  required: true,
                  message: 'Hãy nhập số điện thoại',
                },
              ]}
              className="account-input"
            >
              <Input
                size="large"
                addonBefore={<HeadPhoneNumber />}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Mật khẩu</span>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Hãy nhập password',
                },
              ]}
              hasFeedback
              className="account-input"
            >
              <Input.Password size="large" />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span className="register-label">Nhập lại mật khẩu</span>
            <Form.Item
              name="confirm"
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: 'Hãy nhập password',
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      'Mật khẩu và xác thực mật khẩu không đúng'
                    );
                  },
                }),
              ]}
              hasFeedback
              className="account-input"
            >
              <Input.Password size="large" />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              className="button-form"
              style={{ width: '100%' }}
            >
              Tiếp theo
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
export default RegisterForm;
