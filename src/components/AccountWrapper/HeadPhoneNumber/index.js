import React from 'react';

const HeadPhoneNumber = () => {
  return (
    <span
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <img
        style={{
          width: '2.2rem',
          height: '1.6rem',
          marginRight: '0.4rem',
        }}
        src="/assets/images/vietnam-flag.png"
        alt="vietnam-flag"
      />
      <span
        style={{ fontSize: '1.6rem', fontWeight: '500', lineHeight: '2.25rem' }}
      >
        +84
      </span>
    </span>
  );
};

export default HeadPhoneNumber;
