/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from 'react';
import { Row, Col, Upload, Modal, Image, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import { getBase64File, createFileName } from 'utils/file';

const UploadComponent = (props) => {
  const {
    list = [],
    start = process.env.REACT_APP_BACKEND_URL,
    showButton = true,
  } = props;
  const { confirm } = Modal;
  require('dotenv').config();

  const createFileList = () => {
    const result = [];
    Array?.isArray(list) &&
      list?.forEach((it) => {
        result.push({
          uid: it?.id,
          name: 'image.png',
          status: 'done',
          url: start + (it?.content ?? it?.name),
        });
      });
    return result;
  };

  const [modalConfirmVisible, setModalConfirmVisible] = useState(false);
  const [modalConfirmTitle, setModalConfirmTitle] = useState('');
  const [modalConfirmSrc, setModalConfirmSrc] = useState('');
  const [imageBanner, setImageUpload] = useState(null);

  const [previewVisible, setReviewVisible] = useState(false);
  const [fileList, setFileList] = useState(createFileList());
  const [previewImage, setReviewImage] = useState('');
  const [previewTitle, setReviewTitle] = useState('');

  useEffect(() => {
    setFileList(createFileList());
  }, [list]); // eslint-disable-line react-hooks/exhaustive-deps

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64File(file.originFileObj);
    }
    setReviewImage(file.url);
    setReviewVisible(true);
    setReviewTitle(file.name);
  };

  const handleRequest = async (file) => {
    const fileBase64 = await getBase64File(file.file);
    setImageUpload(file.file);
    setModalConfirmVisible(true);
    setModalConfirmTitle(file.file.name);
    setModalConfirmSrc(fileBase64);
  };

  const handleAddImage = async () => {
    props.handleAddImage({
      name: createFileName(imageBanner.name),
      file: await getBase64File(imageBanner),
    });
    handleCancel();
  };

  const handleCancel = () => {
    setModalConfirmVisible(false);
    setReviewVisible(false);
  };

  const handleRemoveImage = (_) => {
    confirm({
      maskClosable: true,
      okText: 'Có, tôi muốn xoá',
      cancelText: 'Không',
      title: 'Bạn có chắc muốn xoá ảnh này?',
      onOk() {
        props.handleRemoveImage({
          id: _?.uid,
          name: _?.url.replace(process.env.REACT_APP_BACKEND_URL, ''),
        });
      },
    });
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Tải ảnh lên</div>
    </div>
  );

  const UploadModal = (props) => {
    const { type = 'review' } = props;

    return (
      <Modal
        onCancel={handleCancel}
        visible={type === 'review' ? previewVisible : modalConfirmVisible}
        title={
          type === 'review'
            ? previewTitle
            : `Bạn muốn thêm ảnh ${modalConfirmTitle}?`
        }
        footer={
          type === 'review'
            ? null
            : [
                <Button key="back" onClick={handleCancel}>
                  Huỷ
                </Button>,
                <Button key="submit" type="primary" onClick={handleAddImage}>
                  Thêm
                </Button>,
              ]
        }
      >
        <Image
          alt="upload-image"
          style={{ width: '100%' }}
          src={type === 'review' ? previewImage : modalConfirmSrc}
        />
      </Modal>
    );
  };

  return (
    <Row>
      <Col span={24}>
        <Upload
          name="files"
          listType="picture-card"
          fileList={fileList}
          onPreview={handlePreview}
          customRequest={handleRequest}
          onRemove={handleRemoveImage}
          style={{ width: 'auto', display: 'flex' }}
          accept="image/*"
        >
          {showButton ? uploadButton : null}
        </Upload>
        <UploadModal type="upload" />
        <UploadModal type="review" />
      </Col>
    </Row>
  );
};

export default UploadComponent;
