import React, { useEffect } from 'react';
import { Row, Col, Form, Input, Checkbox, Button } from 'antd';
import styled from 'styled-components';

import slugify from 'utils/slugify';
import TitlePage from 'components/TitlePage';

const StyledSpan = styled.span`
  font-size: 18px;
  margin-bottom: 10px;
  &::before {
    content: '${(props) => (props?.required ? '*' : '')}';
    color: red;
    font-size: 18px;
    margin-right: 5px;
  }
`;

const CategoryForm = (props) => {
  const { bySlug = {} } = props;
  require('dotenv').config();
  const [form] = Form.useForm();
  const { setFieldsValue } = form;

  const initBySlug = {
    id: -1,
    name: '',
    slug: '',
    enable: 0,
    showHome: 0,
    showMenu: 0,
  };

  useEffect(() => {
    if (bySlug?.id) {
      setFieldsValue({
        ...bySlug,
      });
    } else {
      setFieldsValue({
        ...initBySlug,
      });
    }
  }, [bySlug]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleFinish = (values) => {
    props.onFinish({
      ...values,
      slug: slugify(values?.slug),
      id: bySlug?.id,
      createBy: bySlug?.createBy,
    });
  };

  return (
    <Row>
      {bySlug?.name && (
        <Col xs={24}>
          <TitlePage title={bySlug?.name.toUpperCase()} />
        </Col>
      )}
      <Col xs={24}>
        <Form form={form} scrollToFirstError onFinish={handleFinish}>
          <Row>
            <Col xs={{ span: 24 }}>
              <Row justify="center" gutter={[{ xs: 0, md: 32 }, 0]}>
                <Col xs={24} sm={12} style={{ marginTop: '2.4rem' }}>
                  <Row>
                    <Col span={24}>
                      <StyledSpan required={true}>Tên danh mục:</StyledSpan>
                      <Form.Item
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng điền tên danh mục sản phẩm',
                          },
                        ]}
                      >
                        <Input size="large" />
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <StyledSpan required={true}>Friendly Url:</StyledSpan>
                      <Form.Item
                        name="slug"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng điền slug',
                          },
                        ]}
                      >
                        <Input size="large" />
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <Form.Item name="enable" valuePropName="checked">
                        <Checkbox>Hiển thị cho người dùng</Checkbox>
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <Form.Item name="showHome" valuePropName="checked">
                        <Checkbox>Hiển thị trên trang chủ</Checkbox>
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <Form.Item name="showMenu" valuePropName="checked">
                        <Checkbox>Hiển thị trên menu</Checkbox>
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>

                <Col xs={{ span: 24 }}>
                  <Button
                    htmlType="submit"
                    type="primary"
                    size="large"
                    style={{ borderRadius: '0.4rem' }}
                  >
                    Lưu danh mục
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  );
};

export default CategoryForm;
