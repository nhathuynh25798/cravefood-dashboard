import React from 'react';
import { notification, Button } from 'antd';

const NotificationRemoveConfirm = (callback) => {
  const key = `open${Date.now()}`;

  const btn = (
    <Button
      type="primary"
      size="medium"
      onClick={() => {
        notification.close(key);
        callback();
      }}
    >
      Xác nhận xoá
    </Button>
  );

  return notification.open({
    message: 'Cảnh báo',
    description: 'Bạn có thực sự muốn xoá lựa chọn này',
    btn,
    key,
  });
};

export default NotificationRemoveConfirm;
