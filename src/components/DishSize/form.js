import React from 'react';
import { Row, Col, Form, Input, Select } from 'antd';
import styled from 'styled-components';

const DishIngredientForm = (props) => {
  const { dishIngredient = {} } = props;
  const { ingredient, ingredientId, unit, quantity } = dishIngredient;
  const [form] = Form.useForm();
  const { Option } = Select;

  const onFinish = (values) => {
    console.log(values);
  };

  const StyledSpan = styled.span`
    font-size: 18px;
    margin-bottom: 10px;
    &::before {
      content: '${(props) => (props?.required ? '*' : '')}';
      color: red;
      font-size: 18px;
      margin-right: 5px;
    }
  `;

  return (
    <Form form={form} scrollToFirstError onFinish={onFinish}>
      <Row>
        <Col xs={{ span: 24 }}>
          <Row justify="space-between" gutter={[{ xs: 0, md: 32 }, 0]}>
            <Col xs={24} sm={12} style={{ marginTop: '2.4rem' }}>
              <Row>
                <Col span={24}>
                  <StyledSpan required={true}>Tên sản phẩm:</StyledSpan>
                  <Form.Item
                    name="ingredientId"
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng điền tên sản phẩm',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
  );
};
