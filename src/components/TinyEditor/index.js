import React, { useEffect, useState, useRef } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

ClassicEditor.defaultConfig = {
  image: {
    toolbar: [
      'imageStyle:alignLeft',
      'imageStyle:full',
      'imageStyle:alignCenter',
      'imageStyle:alignRight',
      '|',
      'imageTextAlternative',
    ],
    styles: ['full', 'alignLeft', 'alignCenter', 'alignRight'],
  },
  table: {
    contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
  },
};

const defaultItems = [
  'heading',
  'bold',
  'italic',
  'link',
  'bulletedList',
  'numberedList',
  'blockQuote',
  'undo',
  'redo',
  '|',
  'insertTable',
];

export default function Index(props) {
  const { oldValue, showImageUpload = false, showMediaEmbed = false } = props;
  const [value, setValue] = useState(oldValue);
  const editorRef = useRef();

  const mediaItems = [];

  if (showImageUpload) {
    mediaItems.push('imageUpload');
  }

  if (showMediaEmbed) {
    mediaItems.push('mediaEmbed');
  }

  useEffect(() => {
    if (!editorRef.current || !editorRef.current.editor) {
      return;
    }

    const currentValue = editorRef.current.editor.getData();
    const newValue = props.data || props.value;

    if (newValue !== currentValue) {
      setValue(newValue || '');
    }
  }, [props.data, props.value]);

  return (
    <CKEditor
      ref={editorRef}
      editor={ClassicEditor}
      data={value}
      config={{
        toolbar: {
          items: [...defaultItems, ...mediaItems],
        },
      }}
      onInit={(editor) => {
        // console.log('init editor....', editor);
        props.onInit && props.onInit(editor);
      }}
      onChange={(event, editor) => {
        const data = editor.getData();
        props.onChange && props.onChange(data);
      }}
      onBlur={(event, editor) => {
        // console.log('Blur.', editor);
      }}
      onFocus={(event, editor) => {
        // console.log('Focus.', editor);
      }}
    />
  );
}
