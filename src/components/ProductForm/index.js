/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Checkbox,
  Button,
  Select,
  Tag,
  Image,
  Modal,
} from 'antd';
import styled from 'styled-components';
import { InputUNumber, PriceNumber } from 'components/Input';
import UploadComponent from 'components/UploadComponent';
import TinyEditor from 'components/TinyEditor';
import { useSelector } from 'react-redux';
import { DeleteFilled } from '@ant-design/icons';

import TitlePage from 'components/TitlePage';

const StyledSpan = styled.span`
  font-size: 18px;
  margin-bottom: 10px;
  &::before {
    content: '${(props) => (props?.required ? '*' : '')}';
    color: red;
    font-size: 18px;
    margin-right: 5px;
  }
`;

const StyledImage = styled(Col)`
  position: relative;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 104px;
  padding: 8px;
  margin-right: 8px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
`;

const tagRender = (props) => {
  const { label, closable, onClose } = props;

  return (
    <Tag
      color="green"
      closable={closable}
      onClose={onClose}
      style={{ marginRight: 3 }}
    >
      {label}
    </Tag>
  );
};

const ProductForm = (props) => {
  const { bySlug = {} } = props;
  require('dotenv').config();
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const { confirm } = Modal;

  const initBySlug = {
    id: -1,
    name: '',
    slug: '',
    enable: 0,
    description: '',
    discount: 0,
    categories: [],
    sizes: [],
    images: [],
    uploads: [],
    selectItems: [],
    createBy: null,
    smallValue: 0,
    mediumValue: 0,
    largeValue: 0,
  };

  const [uploadImages, setUploadImage] = useState([]);
  const [removeImages, setRemoveImages] = useState([]);

  useEffect(() => {
    if (bySlug?.id) {
      setFieldsValue({
        ...bySlug,
        uploads:
          bySlug?.images?.map((it) => {
            return {
              id: it?.id,
              content: process.env.REACT_APP_BACKEND_URL + it?.content,
            };
          }) ?? [],
        selectItems: bySlug?.categories.map((it) => it?.id) ?? [],
        smallValue:
          bySlug?.sizes?.filter((it) => it?.sizeId === 1)?.[0]?.price ?? 0,
        mediumValue:
          bySlug?.sizes?.filter((it) => it?.sizeId === 2)?.[0]?.price ?? 0,
        largeValue:
          bySlug?.sizes?.filter((it) => it?.sizeId === 3)?.[0]?.price ?? 0,
      });
    } else {
      setFieldsValue({
        ...initBySlug,
      });
    }
    setUploadImage([]);
    setRemoveImages([]);
  }, [bySlug]); // eslint-disable-line react-hooks/exhaustive-deps

  const createOptions = (array = []) => {
    const options = [];
    array?.forEach((it) =>
      options?.push({
        value: it?.id,
        label: it?.name,
      })
    );
    return options;
  };

  const handleAddImage = (file) => {
    setUploadImage([...uploadImages, file]);
  };

  const handleRemoveImage = (file) => {
    // dispatch(productActions(actDeleteImage({file})));
    setRemoveImages([...removeImages, file]);
  };

  const listCategory = useSelector(
    (store) => store.globalValue.globalValue.categories ?? []
  );

  const handleFinish = (values) => {
    props.onFinish({
      ...values,
      id: bySlug?.id,
      createBy: bySlug.createdBy,
      uploadImages: uploadImages,
      removeImages: removeImages,
    });
  };

  return (
    <Row>
      {bySlug?.name && (
        <Col xs={24}>
          <TitlePage title={bySlug?.name.toUpperCase()} />
        </Col>
      )}
      <Col xs={24}>
        <Form
          form={form}
          scrollToFirstError
          onFinish={handleFinish}
          initialValues={bySlug}
        >
          <Row>
            <Col xs={{ span: 24 }}>
              <Row justify="space-between" gutter={[{ xs: 0, md: 32 }, 0]}>
                <Col xs={24} sm={12} style={{ marginTop: '2.4rem' }}>
                  <Row>
                    <Col span={24}>
                      <StyledSpan required={true}>Tên sản phẩm:</StyledSpan>
                      <Form.Item
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng điền tên sản phẩm',
                          },
                        ]}
                      >
                        <Input size="large" />
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <StyledSpan required={true}>Friendly Url:</StyledSpan>
                      <Form.Item
                        name="slug"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng điền slug',
                          },
                        ]}
                      >
                        <Input size="large" />
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <Form.Item name="enable" valuePropName="checked">
                        <Checkbox>Hiển thị cho người dùng</Checkbox>
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <StyledSpan>Mô tả chi tiết:</StyledSpan>
                      <Form.Item name="description" valuePropName="value">
                        <TinyEditor />
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>

                <Col xs={24} sm={12} style={{ marginTop: '2.4rem' }}>
                  <Row>
                    <Col span={24}>
                      <StyledSpan>Hình ảnh:</StyledSpan>
                      <Row gutter={[0, 16]}>
                        <Col span={24}>
                          <Form.Item name="uploads" valuePropName="list">
                            <UploadComponent
                              start=""
                              handleAddImage={handleAddImage}
                              handleRemoveImage={handleRemoveImage}
                            />
                          </Form.Item>
                          {Array.isArray(uploadImages) && (
                            <Row>
                              {uploadImages?.map((it) => (
                                <StyledImage>
                                  <Image
                                    src={it?.file}
                                    style={{ border: '1px solid #d9d9d9' }}
                                  />
                                  <div
                                    onClick={() =>
                                      confirm({
                                        maskClosable: true,
                                        okText: 'Có, tôi muốn xoá',
                                        cancelText: 'Không',
                                        title: 'Bạn có chắc muốn xoá ảnh này?',
                                        onOk() {
                                          const uploadIndex = uploadImages?.findIndex(
                                            (item) => item?.file === it?.name
                                          );
                                          uploadImages.splice(uploadIndex, 1);
                                          setUploadImage([...uploadImages]);
                                        },
                                      })
                                    }
                                  >
                                    <DeleteFilled
                                      style={{ marginTop: '5px' }}
                                    />
                                  </div>
                                </StyledImage>
                              ))}
                            </Row>
                          )}
                        </Col>
                      </Row>
                    </Col>

                    <Col span={24}>
                      <StyledSpan>Hệ số giảm giá:</StyledSpan>
                      <Form.Item name="discount">
                        <InputUNumber />
                      </Form.Item>
                    </Col>

                    <Col span={24}>
                      <Row>
                        <Col span={24}>
                          <StyledSpan>Giá:</StyledSpan>
                        </Col>
                        <Col span={24}>
                          <Row gutter={[16, 0]} style={{ padding: '10px 0' }}>
                            <Col span={4}>
                              <span>Size nhỏ:</span>
                            </Col>
                            <Form.Item name="smallValue">
                              <PriceNumber />
                            </Form.Item>
                          </Row>

                          <Row gutter={[16, 0]} style={{ padding: '10px 0' }}>
                            <Col span={4}>
                              <span>Size vừa:</span>
                            </Col>
                            <Form.Item name="mediumValue">
                              <PriceNumber />
                            </Form.Item>
                          </Row>

                          <Row gutter={[16, 0]} style={{ padding: '10px 0' }}>
                            <Col span={4}>
                              <span>Size lớn:</span>
                            </Col>
                            <Form.Item name="largeValue">
                              <PriceNumber />
                            </Form.Item>
                          </Row>
                        </Col>
                      </Row>
                    </Col>

                    <Col span={24}>
                      <Row gutter={[0, 16]}>
                        <Col span={24}>
                          <StyledSpan>Danh mục:</StyledSpan>
                        </Col>
                        <Col span={24}>
                          <Form.Item name="selectItems">
                            <Select
                              mode="multiple"
                              showArrow
                              tagRender={tagRender}
                              style={{ width: '100%' }}
                              options={createOptions(listCategory)}
                            />
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>

                    <Col xs={{ span: 24 }}>
                      <Button
                        htmlType="submit"
                        type="primary"
                        size="large"
                        style={{ borderRadius: '0.4rem' }}
                      >
                        Lưu sản phẩm
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  );
};

export default ProductForm;
