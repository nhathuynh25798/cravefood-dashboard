import React, { useEffect, useState } from 'react';
import { Row, Col, Menu, Dropdown, Modal } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from '@reach/router';
import styled from 'styled-components';

import { actionCreator as userActions } from 'store/user/user.meta';
import { actionCreators } from 'store/globalValue/global.meta';
import './style.scss';

const StyledSpan = styled.span`
  &:hover {
    color: var(--secondary);
  }
`;

const HeaderAdmin = () => {
  const dispatch = useDispatch();
  const { confirm } = Modal;

  const [isUserLogin, setIsUserLogin] = useState(false);

  const { user = {} } = useSelector((store) => store.user);

  const fetchGlobal = () => {
    dispatch(actionCreators.actFetchGlobalValue());
  };

  useEffect(() => {
    if (!!user) {
      setIsUserLogin(true);
    }
  }, [user]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    fetchGlobal();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const DropdownMenu = (props) => {
    const { name, data } = props;

    const menu = (
      <Menu className="sub__menu">
        {data.map((item) => (
          <Menu.Item className="sub__menu-item" key={item?.id}>
            {item?.onClick ? (
              <StyledSpan onClick={item?.onClick}>{item?.name}</StyledSpan>
            ) : (
              <Link to={item?.link}>{item?.name}</Link>
            )}
          </Menu.Item>
        ))}
      </Menu>
    );

    return (
      <Dropdown overlay={menu}>
        <Link to="/" className="menu__item">
          <span style={{ fontSize: '20px', color: 'white' }}>{name}&nbsp;</span>
          <DownOutlined style={{ fontSize: '18px', color: 'white' }} />
        </Link>
      </Dropdown>
    );
  };

  const subMenu = [
    {
      id: 'profile',
      name: 'Tài khoản',
      link: '/profile',
    },
    {
      id: 'logout',
      name: 'Đăng xuất',
      onClick: () => {
        confirm({
          okText: 'Đăng xuất',
          cancelText: 'Hủy thao tác',
          title:
            'Bạn có chắc bạn muốn đăng xuất khỏi tài khoản Quản trị viên của CRAVEFOOD không?',
          onOk() {
            dispatch(userActions.logout());
            setIsUserLogin(false);
          },
        });
      },
    },
  ];

  return (
    <Row
      justify="end"
      style={{
        width: '100%',
        maxWidth: '100%',
        minHeight: '100%',
        padding: '0 40px',
      }}
    >
      {!isUserLogin ? (
        <Col>
          <Link to="/login">
            <UserOutlined style={{ color: 'white', fontSize: '20px' }} />
          </Link>
        </Col>
      ) : (
        <Col>
          <Row align="middle" justify="end" style={{ height: '100%' }}>
            <DropdownMenu name="Xin chào admin" data={subMenu} />
          </Row>
        </Col>
      )}
    </Row>
  );
};

export default HeaderAdmin;
