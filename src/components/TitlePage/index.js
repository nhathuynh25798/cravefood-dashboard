import React from 'react';
import { Row, Col } from 'antd';

const TitlePage = (props) => {
  const { title } = props;

  return (
    <Row>
      <Col xs={12}>
        <h1 style={{ fontSize: 40 }}>{title}</h1>
      </Col>
    </Row>
  );
};

export default TitlePage;
