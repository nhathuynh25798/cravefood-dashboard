import React from 'react'
import { Row, Col } from 'antd'

const FooterAdmin = (props) => {
  return (
    <Row justify='center'>
      <Col>
        <span
          style={{
            color: 'rgba(0,0,0,.65)',
            fontSize: '14px',
          }}
        >
          Cravefood Management System ©2020 Created by NhatHuynh
        </span>
      </Col>
    </Row>
  )
}

export default FooterAdmin
