import { actionCreator } from 'store/page/page.meta';
import { call, put } from 'redux-saga/effects';
import { notification } from 'antd';

const messageReg = {
  KEYWORD_IS_EMPTY: /empty keyword/g,
  SESSION_EXPIRE: /Token is required/g,
  TOKEN_EXPIRE: /token is expired/g,
  PASSWORD_NOT_CORRECT: /hashedPassword is not the hash/g,
  PHONE_IS_NOT_VALID: /{Phone}/g,
  EMAIL_IS_NOT_VALID: /{Email}/g,
  EMAIL_ALREADY_EXISTS: /email is already exist/g,
  USER_NOT_FOUND: /User is not found/g,
  ADDRESS_DEFAULT: /address default not delete/g,
  SLUG_IS_USED: /slug is used/g,
  SLUG_IS_NOT_EXISTS: /this product is not slug/g,
  NAME_IS_USED: /name is used/g,
  NAME_IS_NOT_EXISTS: /this product is not name/g,
};

export const errorMessages = {
  KEYWORD_IS_EMPTY: 'Xin vui lòng nhập từ khoá',
  SESSION_EXPIRE: 'Phiên làm việc hết hạn, vui lòng đăng nhập lại.',
  TOKEN_EXPIRE: 'Phiên làm việc hết hạn',
  DEFAULT: 'Đã có lỗi xảy ra, vui lòng thử lại',
  PASSWORD_NOT_CORRECT:
    'Thông tin đăng nhập không hợp lệ. Xin vui lòng thử lại',
  EMAIL_ALREADY_EXISTS: 'Email đã được đăng ký.',
  USER_NOT_FOUND: 'Thông tin đăng nhập không hợp lệ. Xin vui lòng thử lại',
  PHONE_IS_NOT_VALID: 'Số điện thoại không hợp lệ',
  EMAIL_IS_NOT_VALID: 'Email không hợp lệ',
  ADDRESS_DEFAULT: 'Địa chỉ mặc định không thể xoá!',
  SLUG_IS_USED: 'Đã có mục dùng slug này, hãy thay đổi.',
  SLUG_IS_NOT_EXISTS: 'Mục này bắt buộc phải có slug',
  NAME_IS_USED: 'Đã có mục dùng tên này, hãy thay đổi.',
  NAME_IS_NOT_EXISTS: 'Mục này bắt buộc phải có tên',
};

const errorMessageKeys = Object.keys(messageReg);

export const parseError = (message) => {
  const errorKey = errorMessageKeys.find((key) =>
    message.match(messageReg[key])
  );
  if (errorKey) return errorMessages[errorKey];

  return errorMessages['DEFAULT'];
};

export function sagaErrorWrapper(executor, customErrorHandling) {
  return function* (action) {
    try {
      yield put(actionCreator.setLoading(true));
      yield executor(action);
    } catch (error) {
      const { message, status } = error?.response?.data ?? error;

      if (customErrorHandling) {
        yield call(customErrorHandling, error);
        return;
      }

      if (status === 500) {
        const errorMessage = parseError(message);
        notification.error({
          message: errorMessage,
        });

        return;
      }

      if (status === 401) {
        const errorMessage = parseError(message);
        notification.error({
          message: errorMessage,
        });

        return;
      }

      if (status === 400) {
        const errorMessage = parseError(message);
        notification.error({
          message: errorMessage,
        });

        return;
      }

      notification.error({
        message: errorMessages['DEFAULT'],
      });
    } finally {
      yield put(actionCreator.setLoading(false));
    }
  };
}
