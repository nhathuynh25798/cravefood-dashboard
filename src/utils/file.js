export const getBase64File = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

export const createFileName = (name = 'photo') => {
  const strArr = name.split('.');
  const extension = strArr[strArr.length - 1].trim();
  return `${name.replace(`.${extension}`, '')}-${Math.random()
    .toString(36)
    .substr(2, 9)
    .toUpperCase()}.${extension}`;
};

export const dataURLtoBlob = (dataurl, name) => {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  const blob = new Blob([u8arr], { type: mime });

  return new File([blob], name);
};
