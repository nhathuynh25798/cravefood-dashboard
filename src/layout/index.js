import React from 'react';
import { Layout, Menu, Row, Col } from 'antd';
import { Link } from '@reach/router';

import Routes from '../routes';
import HeaderAdmin from '../components/HeaderAdmin';
import FooterAdmin from '../components/FooterAdmin';

import 'antd/dist/antd.css';
import './style.scss';

const { Header, Footer, Sider, Content } = Layout;
const { SubMenu } = Menu;

const SideBarAdmin = () => {
  const menus = [
    {
      key: 'sub1',
      title: 'Người dùng',
      icon: '/assets/images/user-icon.svg',
      menuItems: [
        // {
        //   key: '1',
        //   title: 'Quản trị viên',
        // },
        {
          key: '2',
          title: 'Khách hàng',
          link: '/users/1',
        },
      ],
    },
    {
      key: 'sub2',
      title: 'Sản phẩm',
      icon: '/assets/images/product-icon.svg',
      menuItems: [
        // {
        //   title: 'Các nguyên liệu',
        //   link: '/ingredients',
        // },
        {
          key: '3',
          title: 'Danh mục món ăn',
          link: '/categories/1',
        },
        {
          key: '4',
          title: 'Danh sách các món ăn',
          link: '/products/1',
        },
      ],
    },
    {
      key: 'sub3',
      title: 'Đơn hàng',
      icon: '/assets/images/order-icon.svg',
      menuItems: [
        {
          key: '6',
          title: 'Quản lý đơn hàng',
          link: '/orders/1',
        },
      ],
    },
    {
      key: 'sub4',
      title: 'Cài đặt',
      icon: '/assets/images/setting-icon.svg',
      menuItems: [
        {
          key: '7',
          title: 'Quản lý Banner',
          link: '/banner',
        },
        // {
        //   key: '8',
        //   title: 'Liên hệ',
        // },
      ],
    },
  ];

  return (
    <Row>
      <Col xs={24}>
        <Row className="dashboard__logo">
          <Col xs={24}>
            <div className="logo-title">
              <span className="name-title">Cravefood</span>
              <span className="des-title">Management</span>
            </div>
          </Col>
        </Row>
        <Menu style={{ width: '100%' }} mode="inline">
          {menus?.map((item) => (
            <SubMenu
              key={item?.key}
              className="submenu-item"
              icon={
                <img
                  src={item?.icon}
                  alt="icon-img"
                  style={{ margin: '0 10px' }}
                />
              }
              title={<strong>{item?.title}</strong>}
            >
              {item?.menuItems.map((it) => (
                <Menu.Item key={it?.link ?? it?.key}>
                  <Link to={it?.link ?? it?.key}>{it?.title}</Link>
                </Menu.Item>
              ))}
            </SubMenu>
          ))}
        </Menu>
      </Col>
    </Row>
  );
};

const LayoutAdmin = () => {
  return (
    <Layout className="layout-container">
      <Sider
        theme="light"
        mode="inline"
        style={{
          position: 'fixed',
          overflow: 'auto',
          left: 0,
          height: '100vh',
        }}
      >
        <SideBarAdmin />
      </Sider>
      <Layout style={{ marginLeft: '230px' }}>
        <Header className="header-container">
          <HeaderAdmin />
        </Header>
        <Content className="content-container">
          <Routes />
        </Content>
        <Footer className="footer-container">
          <FooterAdmin />
        </Footer>
      </Layout>
    </Layout>
  );
};

export default LayoutAdmin;
